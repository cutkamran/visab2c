﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="VisaImmigrationSingle.aspx.cs" Inherits="VisaB2C.VisaImmigrationSingle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="script/Login.js"></script>
    <script src="script/AddVisaDetails.js?v=1.1"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <input type="hidden" id="Name" />
        <input type="hidden" id="Price" />
    <input type="hidden" id="Country" />
    <!-- /.header classic -->
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <h1 class="page-title">United Arab Emirates Visa</h1>
                    <p class="page-description"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">About United Arab Emirates Visa</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page breadcrumb -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="content-area">
                        <img src="images/SLIDER/qatar.jpg" alt="" class="img-fluid mb30">
                        <h2>Dubai Visa</h2>
                        <h3>Whom is a tourist visa for?</h3>
                        <p>Tourist visa is for those who are not eligible for visa on arrival or a visa-free entry to the UAE. Tourist visa can be obtained for eligible individual tourists from around the world. Females below the age of 18 are not eligible to apply for this type of visa unless they are travelling with their parents.</p>
                        <h3>How to Apply</h3>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="sidebar">
                        <div class="widget-testimonial">
                            <div class="card-testimonial card-testimonial-light">
                                <!-- card testimonial start -->
                                <div class="card-testimonial-img">
                                    <img src="images/card-testimonial-img-1.jpg" alt="" class="rounded-circle">
                                </div>
                                <div class="card-testimonial-content">
                                    <p class="card-testimonial-text">“Fusce non mi at nisl laoreet pretiumulla ut elementum sapien, a pulvinar augueed semper sed tellus in ultrices am simply dummy content hendrerit elit vel urna fermentum congue. . ”</p>
                                </div>
                                <div class="card-testimonial-info">
                                    <span class="card-testimonial-flag">
                                        <img src="images/country/canada.svg" class="flag-small"></span>
                                    <h4 class="card-testimonial-name">Dustin A. Morgan</h4>
                                    <p><small>( Canada Students Visa )</small></p>
                                </div>
                            </div>
                            <!-- /.card testimonial start -->
                        </div>
                        <a href="#" onclick="SignInModel('30 Days Multiple Entry','20000','United Arab')" class="btn btn-default">Apply For Visa</a>
                       
                    </div>
                </div>
            </div>
          <%--  <div class="row">
                <div class="content-area">
                    <h3>What is the validity of a tourist visa?</h3>
                    <p>Depending on your plan, tourist visas to the UAE can be issued for 14 days, 30 days or 90 days duration, single entry or multiple entry.</p>
                    <h3>Who can apply for your tourist visa?</h3>
                    <p>The UAE embassies do not issue tourist visas. In order to get a tourist visa to the UAE, you need to click here and we will apply for visa on your behalf to the official visa-issuing authorities in the UAE.</p>
                    <h3>What if someone over stay the period granted?</h3>
                    <p>Visa over stayers will have to pay AED100 fine for each day of their overstay, to be calculated from first day after the visa expiry. After 5 days over stayers will be filed as absconded, and once arrested by authorities will be jailed and will have to pay AED6000 fine plus AED100 each day over stayed and over stayer will be blacklisted from UAE will not be able to revisit the country.</p>

                </div>
            </div>--%>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">1</h2>
                            <p>Fill up short information and upload your documents</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">2</h2>
                            <p>Your application will be reviewed and processed</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">3</h2>
                            <p>You will be notified once visa is issued,you can print e-visa & fly</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
            </div>
        </div>
        <div class="space-medium pdb0">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-1 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="faq-header">
                            <h2 class="faq-title">Visa &amp; Immigration</h2>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-xl-1 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div id="accordion-2">
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingFour">
                                    <h5 class="mb0">
                                        <button class="accordion-btn collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            What is the validity of a tourist visa?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <p>Depending on your plan, tourist visas to the UAE can be issued for 14 days, 30 days or 90 days duration, single entry or multiple entry.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingFive">
                                    <h5 class="mb0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            Who can apply for your tourist visa?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <p>The UAE embassies do not issue tourist visas. In order to get a tourist visa to the UAE, you need to click here and we will apply for visa on your behalf to the official visa-issuing authorities in the UAE.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingSix">
                                    <h5 class="mb-0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            What if someone over stay the period granted?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <p>Visa over stayers will have to pay AED100 fine for each day of their overstay, to be calculated from first day after the visa expiry. After 5 days over stayers will be filed as absconded, and once arrested by authorities will be jailed and will have to pay AED6000 fine plus AED100 each day over stayed and over stayer will be blacklisted from UAE will not be able to revisit the country.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

      <script>
        function SignInModel(Name, Price, Country) {
            
            $("#Name").val(Name);
            $("#Price").val(Price);
            $("#Country").val(Country);
            CheckSession();
        }
    </script>
    <script>
        function GetModel() {
            $("#ModelLogin").modal('hide');
            $("#ModelForgetPass").modal('show');
        }
       
    </script>



</asp:Content>
