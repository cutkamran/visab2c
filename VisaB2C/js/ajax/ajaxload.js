﻿/*get and post*/
function post(posturl, postdata, successfunction, errorfunction) {
    debugger
    $.ajax({
        type: 'POST',
        url: posturl,
        data: JSON.stringify(postdata),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1)
                successfunction(result);
            else
                errorfunction(result);
        },
        error: function (errdata) {
            if (errorfunction) {
                errorfunction(errdata)
            }
            else {
                console.log("Error Occured: ");
                console.log(errdata);
            }
        }
    });
}
/*get and post*/
function postasync(posturl, postdata, successfunction, errorfunction) {
    debugger
    $.ajax({
        type: 'POST',
        url: posturl,
        async: true,
        data: JSON.stringify(postdata),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            result = JSON.parse(result.Result)
            if (result.retCode == 1)
                successfunction(result);
            else
                errorfunction(result);
        },
        error: function (errdata) {
            if (errorfunction) {
                errorfunction(errdata)
            }
            else {
                console.log("Error Occured: ");
                console.log(errdata);
            }
        }
    });
}
function get(posturl, postdata, successfunction, errorfunction) {
    'use strict';
    $.ajax({
        url: posturl,
        type: 'Get',
        cache: false,
        data: postdata,
        success: function (data) {
            successfunction(data)
        },
        error: function (errdata) {
            if (errorfunction) {
                errorfunction(errdata)
            }
            else {
                console.log("Error Occured: ");
                console.log(errdata);
            }
        }
    });
}
