﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="VisaB2C.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/google-map.js"></script>
    <link href="css/intlTelInput.css" rel="stylesheet" />
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZib4Lvp0g1L8eskVBFJ0SEbnENB6cJ-g&callback=initMap">
    </script>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--%>
    <!-- Google Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700%7cPT+Serif:400,400i,700,700i" rel="stylesheet" />
    <!-- Style CSS -->
    <script src="script/ContactUs.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <br />
    <br />
    <br />
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contact us</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page breadcrumb -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-7 col-sm-12 col-12">
                    <div class="contact-form-head">
                        <h2 class="mb0">Leave a Message Here</h2>
                        <p>We will connect you to Visacafe Consultant ASAP.</p>
                    </div>
                    <div class="contact-form mt30">
                        <form method="post" action="contactus.php">
                            <div class="form-group">
                                <label for="name">Name <span class="required">*</span></label>
                                <input type="text" class="form-control" id="txt_name" name="name" placeholder="Your Name" required />
                            </div>
                            <div class="form-group" >
                                <label for="mobileno">Mobile No <span class="required">*</span></label><br />
                                <input type="tel" class="form-control" style="padding-right:207px;" id="txt_mobile" name="mobileno" placeholder="Mobile No" required />

                            </div>
                            <div class="form-group">
                                <label for="email">Email Address <span class="required">*</span></label>
                                <input type="email" class="form-control" id="txt_email" name="email" placeholder="Enter Email" required />
                            </div>
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" id="txt_subject" name="subject" placeholder="Subject" />
                            </div>
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" id="txt_message" name="message" rows="3"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="button" onclick="EmailSend()" class="btn btn-default">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-5 col-md-5 col-sm-12 col-12">
                    <h2>Quick Contact</h2>
                    <p>We are pleased to speak with you to discuss your qualifications and options under the various immigration programs and answer any questions or concerns you may have.</p>
                    <h4 class="mb0">Phone</h4>
                    <p>000-123 4567, 001-123 7654, +0 9912347890</p>
                    <div class="row mb30">
                        <div class="col">
                            <h4 class="mb0">Email</h4>
                            <p>Visacafe2080@gmail.com</p>
                        </div>
                    </div>
                    <div class="row mb20">
                        <div class="col mb10">
                            <h4 class="mb0">Inquire with us</h4>
                            <p>info@Visacafe.com</p>
                        </div>
                        <div class="col mb10">
                            <h4 class="mb0">Send your feedback</h4>
                            <p>support@Visacafe.com</p>
                        </div>
                    </div>
                    <div class="row mb20">
                        <div class="col mb10">
                            <h4 class="mb0">Work with us</h4>
                            <p>career@Visacafe.com</p>
                        </div>
                        <div class="col mb10">
                            <h4 class="mb0">For Alliance with us</h4>
                            <p>alliance@Visacafe.com</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h4 class="mb0">Head Office</h4>
                            <p>3356 Biddie Lane Richmond, VA 23224</p>
                            <a href="#" onclick="AssesmentModel()" class="btn-link-primary">Book free assesment</a>
                        </div>
                        <div class="col">
                            <a href="#" class="btn-link-primary">View our all office & locations</a>
                        </div>
                        <%--  <div class="form-group">
                            <a href="#" onclick="AssesmentModel()" class="btn-link-primary">Book free assesment</a>
                            </div>--%>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ModelBookfreeassesment" role="dialog" aria-labelledby="gridSystemModalLabel" style="padding-right: 30px;" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header" style="border-top: 3px solid #dc2e2f;">
                            <h3 class="modal-title">Book My Free Assesment</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12 row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputEmail2">Your Name</label>
                                        <input type="email" class="form-control" id="txt_assesmentname" placeholder="Your Name" required="">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                        <input type="email" class="form-control" id="txt_freeemail" placeholder="Email Address" required="">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputEmail2">mobileno</label>
                                        <input type="email" class="form-control" id="txt_mobileno" placeholder="Mobile No" required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="selectVisa" class="sr-only">Select Visa</label>
                                        <select class="form-control" id="selectVisa" name="selectVisa">
                                            <option value="Select Visa">Select Visa</option>
                                            <option value="Dubai 30 days single entry Visa">Dubai 30 days single entry Visa</option>
                                            <option value="Dubai 30 days multiple entry Visa">Dubai 30 days multiple entry Visa</option>
                                            <option value="Dubai 90 days single entry Visa">Dubai 90 days single entry Visa</option>
                                            <option value="Dubai 90 days multiple entry Visa">Dubai 90 days multiple entry Visa </option>
                                            <option value="Dubai Transit Visa 96 hours">Dubai Transit Visa 96 hours</option>
                                            <option value="Malaysia E-entry 15 Days Visa">Malaysia E-entry 15 Days Visa </option>
                                            <option value="Malaysia E 30 days Visa">Malaysia E 30 days Visa </option>
                                            <option value="Oman 10 Days Tourist Single Entry Visa">Oman 10 Days Tourist Single Entry Visa</option>
                                            <option value="Oman 10 Days Tourist Single Entry Visa">Oman 30 Days Tourist Single Entry Visa</option>
                                            <option value="Oman 01 Yr Tourist Single Entry Visa">Oman 01 Yr Tourist Multiple Entry Visa</option>
                                            <option value="14 days single bahrain visa">14 Days Single Entry Bahrain Tourist Visa</option>
                                            <option value="14 days single bahrain visa">14 Days multiple Entry Bahrain Tourist Visa</option>
                                            <option value="30 days single qatar visa">30 Days single Entry Qatar (below 59yr) Visa</option>
                                            <option value="30 days single qatar visa">30 Days single Entry Qatar (above 59yr) Visa</option>
                                            <option value="thailand visa">Thailand Visa</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="message">Message</label>
                                        <div class="">
                                            <textarea class="form-control" id="message" name="message" rows="4" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="button" style="float: right" class="btn btn-default btn-sm" onclick="BookAssesment()">Book My Free Assesment</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
    <div id="map"></div>
    <script>
        function AssesmentModel() {

            $("#ModelBookfreeassesment").modal('show');
        }

    </script>
    <script>
        $(function () {
            $("#txt_mobile").intlTelInput({
            });
        })
    </script>
    <script src="js/intlTelInput.js"></script>
</asp:Content>
