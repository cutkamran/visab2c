﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="VisaB2C.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="script/Login.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="Name" />
        <input type="hidden" id="Price" />
    <input type="hidden" id="Country" />
    <div class="slider">
        <!-- slider -->
        <div class="slider-carsoule owl-carousel owl-theme" style="z-index: 0">
            <div class="item">
                <img src="images/SLIDER/dubai.jpg" alt="Immigration Consulting Responsive " />
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-8 col-md-9 col-8">
                            <div class="slider-captions">
                                <h1 class="slider-title dotted-line">United Arab Emirate Visa </h1>
                                <p class="slider-text">Rate Starting <i class="fa fa-rupee"></i>6,800 /-</p>
                                <%--<a href="#" class="btn btn-default btn-lg">Start Coaching Today</a>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%-- <div class="item">
                <img src="images/SLIDER/hongkong.jpg" alt="Immigration Visa consultant HTML5 Template" />
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-8 col-md-9 col-8">
                            <div class="slider-captions">
                                <h1 class="slider-title dotted-line">China Visa </h1>
                                <p class="slider-text">Proin venenatis orci felis, tincidunt sagittis mi lacinia vitae. Vestibulum erat nisi, tincidunt vel lobortis eu, imperdiet eu sapien.</p>
                                 <a href="#" class="btn btn-default btn-lg">BOOK A CONSULTATION</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
            <div class="item">
                <img src="images/SLIDER/malaysia.jpg" alt="Immigration Consulting Bootstrap Responsive" />
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-8 col-md-9 col-8">
                            <div class="slider-captions">
                                <h1 class="slider-title dotted-line">Malaysia Visa</h1>
                                <p class="slider-text">Rate Starting <i class="fa fa-rupee"></i>3,000 /-</p>
                                <%-- <a href="#" class="btn btn-default btn-lg">BOOK A CONSULTATION</a>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="images/SLIDER/thailand.jpg" alt="Immigration Consulting Bootstrap Responsive" />
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-8 col-md-9 col-8">
                            <div class="slider-captions">
                                <h1 class="slider-title dotted-line">Thailand Visa </h1>
                                <p class="slider-text">Rate Starting <i class="fa fa-rupee"></i>3,000 /-</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--  <div class="item">
                <img src="images/SLIDER/singapore.jpg" alt="Immigration Visa consultant HTML5 Template" />
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-8 col-md-9 col-8">
                            <div class="slider-captions">
                                <h1 class="slider-title dotted-line">Singapore Visa</h1>
                                <p class="slider-text">Singapore Visa</p>
                                 <a href="#" class="btn btn-default btn-lg">BOOK A CONSULTATION</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
            <div class="item">
                <img src="images/SLIDER/oman.jpg" alt="Immigration Consulting Responsive " />
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-8 col-md-9 col-8">
                            <div class="slider-captions">
                                <h1 class="slider-title dotted-line">Oman Visa</h1>
                                <p class="slider-text">Rate Starting <i class="fa fa-rupee"></i>6,000 /-</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="images/SLIDER/bahrain.jpg" alt="Immigration Visa consultant HTML5 Template" />
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-8 col-md-9 col-8">
                            <div class="slider-captions">
                                <h1 class="slider-title dotted-line">Bahrain Visa</h1>
                                <p class="slider-text" style="font-size: large;">Rate Starting <i class="fa fa-rupee"></i>9,000 /-</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="images/SLIDER/qatar.jpg" alt="Immigration Visa consultant HTML5 Template" />
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-sm-8 col-md-9 col-8">
                            <div class="slider-captions">
                                <h1 class="slider-title dotted-line">Qatar Visa</h1>
                                <p class="slider-text">Rate Starting <i class="fa fa-rupee"></i>6,000 /-</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider -->
    <%-- <div class="slanting-pattern"></div>--%>

    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-sm-12 col-md-12 col-12">
                <div class="Visa-section Visa-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-pills" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="united-arab-emirates-tab" data-toggle="tab" href="#united-arab-emirates" role="tab" aria-controls="united-arab-emirates" aria-selected="true"><span>
                                <img src="images/country/united-arab-emirates.svg" alt="" class="flag-xs"></span> United Arab Emirates</a>
                        </li>
                        <%-- <li class="nav-item">
                                <a class="nav-link" id="china-tab" data-toggle="tab" href="#china" role="tab" aria-controls="china" aria-selected="false"><span>
                                    <img src="images/country/china.svg" alt="" class="flag-xs"></span>China</a>
                            </li>--%>
                        <li class="nav-item">
                            <a class="nav-link" id="malaysia-tab" data-toggle="tab" href="#malaysia" role="tab" aria-controls="malaysia" aria-selected="false"><span>
                                <img src="images/country/malaysia.svg" alt="" class="flag-xs"></span>Malaysia</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="thailand-tab" data-toggle="tab" href="#thailand" role="tab" aria-controls="thailand" aria-selected="false"><span>
                                <img src="images/country/thailand.svg" alt="" class="flag-xs"></span>Thailand</a>
                        </li>
                        <%--   <li class="nav-item">
                                <a class="nav-link" id="singapore-tab" data-toggle="tab" href="#singapore" role="tab" aria-controls="singapore" aria-selected="false"><span>
                                    <img src="images/country/singapore.svg" alt="" class="flag-xs"></span>Singapore</a>
                            </li>--%>
                        <li class="nav-item">
                            <a class="nav-link" id="oman-tab" data-toggle="tab" href="#oman" role="tab" aria-controls="oman" aria-selected="false"><span>
                                <img src="images/country/oman.svg" alt="" class="flag-xs"></span>Oman</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Bahrain-tab" data-toggle="tab" href="#Bahrain" role="tab" aria-controls="Bahrain" aria-selected="false"><span>
                                <img src="images/country/bahrain.svg" alt="" class="flag-xs"></span>Bahrain</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Qatar-tab" data-toggle="tab" href="#Qatar" role="tab" aria-controls="Qatar" aria-selected="false"><span>
                                <img src="images/country/qatar.svg" alt="" class="flag-xs"></span>Qatar</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active fade show" id="united-arab-emirates" role="tabpanel" aria-labelledby="united-arab-emirates-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <%-- <div class="Visa-card-img zoomimg">
                                                <a href="#">
                                                    <img src="images/Visaimage/dubai1.jpg" alt="" class="img-fluid"></a>
                                            </div>--%>
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">30 Days Single Entry</a></h3>
                                            <p><i class="fa fa-rupee"></i>6,800 /- </p>
                                            <a href="dubai30days-singleentry-visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('30 Days Single Entry','6800','United Arab')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">30 Days Multiple Entry</a></h3>
                                            <p><i class="fa fa-rupee"></i>20,000 /-</p>
                                            <a href="dubai-30days-multipleentry-Visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('30 Days Multiple Entry','20000','United Arab')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">90 Days Single Entry</a></h3>
                                            <p><i class="fa fa-rupee"></i>19,000 /-</p>
                                            <a href="Dubai-90-Days-SingleEntry-visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('90 Days Single Entry','19000','United Arab')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">90 Days Multiple Entry </a></h3>
                                            <p><i class="fa fa-rupee"></i>40,000/-</p>
                                            <a href="dubai-90days-multipleentry-visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('90 Days Multiple Entry','40000','United Arab')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">Transit Visa 96 Hours</a></h3>
                                            <p><i class="fa fa-rupee"></i>5,500 /-</p>
                                            <a href="Dubai-Transit-visa-96-hours.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('Transit Visa 96 Hours','5500','United Arab')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <%-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Service Visa 14 Days</a></h3>
                                                <p><em>Rs.</em>6556/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>--%>
                            </div>
                        </div>
                        <%--  <div class="tab-pane fade" id="china" role="tabpanel" aria-labelledby="china-tab">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Single Entry With Doc</a></h3>
                                                <p><em>Rs.</em> 5,900/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                        <div class="Visa-card">

                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Double Entry With Doc</a></h3>
                                                <p><em>Rs.</em>7,900/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                        <div class="Visa-card">

                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Multiple 1yrs With Doc</a></h3>
                                                <p><em>Rs.</em>14,200/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Multiple 6mts With Doc</a></h3>
                                                <p><em>Rs.</em>10,900/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <%-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-img zoomimg">
                                                <a href="#">
                                                    <img src="images/Visa-img-1.jpg" alt="" class="img-fluid"></a>
                                            </div>
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Single Entry Without Doc</a></h3>
                                                <p><em>Rs.</em>6,800/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a href="#" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>--%>
                        <%--  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-img zoomimg">
                                                <a href="#">
                                                    <img src="images/Visa-img-3.jpg" alt="" class="img-fluid"></a>
                                            </div>
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Double Entry Without Doc</a></h3>
                                                <p><em>Rs.</em>9,000/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a href="#" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>  --%>
                        <%--        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-img zoomimg">
                                                <a href="#">
                                                    <img src="images/Visa-img-3.jpg" alt="" class="img-fluid"></a>
                                            </div>
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Multiple 1yrs Without Doc</a></h3>
                                                <p><em>Rs.</em>19,000/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a href="#" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>--%>
                        <%-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">For all above Visas for Urgent </a></h3>
                                                <p><em>Rs.</em>2,700/- Extra</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                        <div class="tab-pane fade" id="malaysia" role="tabpanel" aria-labelledby="malaysia-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">E entry 15 Days</a></h3>
                                            <p><i class="fa fa-rupee"></i>3,000 /-</p>
                                            <a href="Malaysia-E-Entry-15Days-Visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('E entry 15 Days','3000','Malaysia')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">E Visa 30 Days</a></h3>
                                            <p><i class="fa fa-rupee"></i>4,500 /-</p>
                                            <a href="Malaysia-E-30Days-Visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('E Visa 30 Days','4500','Malaysia')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <%--  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Malaysia Sticker Visa</a></h3>
                                                <p><em>Rs.</em>7370/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>--%>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="thailand" role="tabpanel" aria-labelledby="thailand-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">With Doc</a></h3>
                                            <p><i class="fa fa-rupee"></i>3,000 /-</p>
                                            <a href="ThailandVisa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('With Doc','3000','Thailand')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <%--  <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-img zoomimg">
                                                <a href="#">
                                                    <img src="images/Visa-img-1.jpg" alt="" class="img-fluid"></a>
                                            </div>
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Without Doc</a></h3>
                                                <p><em>Rs.</em>500/- Extra</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                 <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a href="#" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>--%>
                            </div>
                        </div>
                        <%--  <div class="tab-pane fade" id="singapore" role="tabpanel" aria-labelledby="singapore-tab">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                        <div class="Visa-card">
                                            <div class="Visa-card-content">
                                                <h3 class="Visa-card-title"><a href="#" class="title">Singapore Visa</a></h3>
                                                <p><em>Rs.</em>3050/-</p>
                                                <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                                <span>
                                                    <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                                </span>
                                                <span><a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a></span>
                                            </div>
                                            <div class="slanting-pattern-small"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>--%>
                        <div class="tab-pane fade" id="oman" role="tabpanel" aria-labelledby="oman-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">Oman 10 Days Tourist Visa Single Entry</a></h3>
                                            <p><i class="fa fa-rupee"></i>6,000 /-</p>
                                            <a href="oman-10days-visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('Oman 10 Days Tourist Visa Single Entry','6000','Oman')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">Oman 30 Days Tourist Visa Single Entry</a></h3>
                                            <p><i class="fa fa-rupee"></i>8500/-</p>
                                            <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('Oman 30 Days Tourist Visa Single Entry','8500','Oman')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">Oman 01 Year Tourist Visa Multiple Entry</a></h3>
                                            <p><i class="fa fa-rupee"></i>16,000 /-</p>
                                            <a href="oman-1yr-visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('Oman 01 Year Tourist Visa Multiple Entry','16000','Oman')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Bahrain" role="tabpanel" aria-labelledby="Bahrain-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">14 Days Single Entry Bahrain Tourist Visa</a></h3>
                                            <p><i class="fa fa-rupee"></i>9,000 /-</p>
                                            <a href="14-Days-Bahrain-visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('14 Days Single Entry Bahrain Tourist Visa','9000','Bahrain')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">14 Days Multiple Entry Bahrain Tourist Visa</a></h3>
                                            <p><i class="fa fa-rupee"></i>12,000 /-</p>
                                            <a href="14days-bahrain-multiple-visa.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('14 Days Multiple Entry Bahrain Tourist Visa','12000','Bahrain')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Qatar" role="tabpanel" aria-labelledby="Qatar-tab">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">30 Days Single Entry Qatar Tourist Visa</a></h3>
                                            <p>( Below 59 yrs)</p>
                                            <p><i class="fa fa-rupee"></i>6,000/-</p>
                                            <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('30 Days Single Entry Qatar Tourist Visa','6000','Qatar')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                                    <div class="Visa-card">
                                        <div class="Visa-card-content">
                                            <h3 class="Visa-card-title"><a href="#" class="title">30 Days Single Entry Qatar Tourist Visa</a></h3>
                                            <p>( Above 59 yrs)</p>
                                            <p><i class="fa fa-rupee"></i>8,500 /-</p>
                                            <a href="VisaImmigrationSingle.aspx" class="btn-link-primary">Read More</a>
                                            <span>
                                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                                            </span>
                                            <span>
                                                <%--<a onclick="SignInModel()" class="btn btn-default btn-sm">Apply Now</a>--%>
                                                <button type="button" onclick="SignInModel('30 Days Single Entry Qatar Tourist Visa','8500''Qatar')" class="btn btn-default btn-sm">Apply Now</button>
                                            </span>
                                        </div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="space-small bg-light">

        <!-- Service section -->

        <div class="container">

            <div class="">

                <div class="">

                    <div class="row">

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">

                            <div class="impact-block">

                                <div class="impact-icon mb40 icon-default"><i class="icon-student-1"></i></div>

                                <h2 class="impact-title">12+</h2>

                                <p class="impact-text">Years experience</p>

                                <p>We are well expreanced to handle all your visa requirments, our dedicated team is always making sure we deliver what you expect...</p>



                            </div>

                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">

                            <div class="impact-block">

                                <div class="impact-icon mb40 icon-default"><i class="icon-diploma"></i></div>

                                <h2 class="impact-title">112,158</h2>

                                <p class="impact-text">Visa processed</p>

                                <p>While you submit your visa request we may have processed tens of new visa, our market reach is better then others...</p>

                            </div>

                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">

                            <div class="impact-block">

                                <div class="impact-icon mb40 icon-default"><i class="icon-university"></i></div>

                                <h2 class="impact-title">40+</h2>

                                <p class="impact-text">Visa types</p>

                                <p>there are more than 40+ visa types in 20+ countries you can apply, 15 online &amp; for rest contct our visa experts</p>



                            </div>

                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">

                            <div class="impact-block">

                                <div class="impact-icon mb40 icon-default"><i class="icon-earth-globe"></i></div>

                                <h2 class="impact-title">6</h2>

                                <p class="impact-text">Countries</p>

                                <p>We are providing services for more then 20 countries supporting e-visa, currently apply for 6 online for others contact us</p>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- /.Service section -->
    <div class="space-medium">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-7 col-sm-6 col-12">
                    <h2>Why Vizacafe ?</h2>
                   <%-- <p class="lead">Trusted by 1224+ clients from 20 countries</p>--%>
                    <p>We are trusted visa consultant by thousands of clients who have repeatedly chosen us to handle their sensitive documents.</p>
                    <ul class="check-circle listnone">
                        <li>Easy, Fast & Secure online processing</li>
                        <li>Serving for more than 12 years</li>
                        <li>Refund your visa charges if your visa is not accepted by immigration</li>
                        <li>Minimum service charge and maximum service quality</li>
                        <li>Almost 98% success rate
                        </li>
                        <li>Instant support</li>
                    </ul>
                    <a href="AboutUs.aspx" class="btn btn-default">About Viza cafe</a> <a href="ContactUs.aspx" class="btn btn-primary">Contact us</a>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-5 col-sm-6 col-12">
                    <div class="img-block">
                        <img src="images/about-fancy-img-1.png" alt="simple bootstrap template" class="img-fluid">
                        <span><a href="https://www.youtube.com/channel/UCJUtR8T3rX0iFDttSFZ8fbQ" class="btn-round-play video" id=""><i class="fa fa-play"></i></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="space-medium">
        <div class="container">
            <div class="row">
                <div class="offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title text-center mb60">
                        <!-- section title start-->
                        <h2>What our Clients say about us. Client Testimonials</h2>
                        <!-- /.section title start-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img">
                            <img src="images/card-testimonial-img-1.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">“Fusce non mi at nisl laoreet pretiumulla ut elementum sapien, a pulvinar augueed semper sed tellus in ultrices am simply dummy content hendrerit elit vel urna fermentum congue. . ”</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag">
                                <img src="images/country/united-arab-emirates.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Dustin A. Morgan</h4>
                            <p><small>( united-arab-emirates Students Visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img">
                            <img src="images/card-testimonial-img-2.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">“Nulla facilisi. Phasellus maximus odio bibendum tortor scelerisque, quis fermentum nulla ornare. Praesent malesuada lorem nec mauris vulputate, viverra tempus felis consequat.”</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag">
                                <img src="images/country/united-states-of-america.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Sandra M. Lebrun</h4>
                            <p><small>( thailand PR Visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img">
                            <img src="images/card-testimonial-img-3.jpg" alt="" class="rounded-circle">
                        </div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">“Sed sollicitudin, ex id lacinia sagittis, lacus lacus ruNulla in ex ut orci pretium congue quis id nunc. trum lus nulla nunc rutrum est. Pellentesque sit amet elementum risus.”</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag">
                                <img src="images/country/malaysia.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Roger N. Towle</h4>
                            <p><small>( UK Tier 1 Visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>
            </div>
        </div>
    </div>
     <div class="space-medium bg-light">
        <!-- News section -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="section-title mb60">
                        <!-- section title start-->
                        <h2>Resources &amp; News</h2>
                    </div>
                    <!-- /.section title start-->
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="post-holder">
                        <div class="post-img zoomimg">
                            <a>
                                <img src="images/resource1.jpg" alt="Visa Immigrations Consultant Responsive Web Design Templates" class="img-fluid"></a>
                        </div>
                        <div class="post-header">
                            <h2 class="post-title"><a href="#" class="title">You can now apply for long-term UAE visas.</a></h2>
                            <p class="meta"><span class="meta-date">1 Feb, 2018</span> <span class="meta-author">By <a href="#">Vizacafe</a></span> <span class="meta-comments"><a>Comments</a></span></p>
                        </div>
                        <div class="post-content">
                            <p>UAE Cabinet approved 5-year, 10-year visas for investors, entrepreneurs, talented expats</p>
                            <a  class="btn btn-default">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="post-holder">
                        <div class="post-img zoomimg">
                            <a>
                                <img src="images/resource2.jpg" alt="Visa Immigrations Consultant Responsive Web Design Templates" class="img-fluid"></a>
                        </div>
                        <div class="post-header">
                            <h2 class="post-title"><a href="#" class="title">Amnesty: What happens if an outpass expires ?</a></h2>
                            <p class="meta"><span class="meta-date">5 Oct, 2018</span> <span class="meta-author">By <a href="#">Vizacafe</a></span> <span class="meta-comments"><a>Comments</a></span></p>
                        </div>
                        <div class="post-content">
                            <p>Lakshmi Devi Reddy, a housemaid who ran away from her employer, faces this conundrum.</p>
                            <a  class="btn btn-default">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="post-holder">
                        <div class="post-img zoomimg mb-4">
                            <a>
                                <img src="images/Resource3.jpg" alt="resource" class="img-fluid"></a>
                        </div>
                        <div class="post-header">
                            <h2 class="post-title"><a href="#" class="title">UAE gets ready to implement visa reforms</a></h2>
                            <p class="meta"><span class="meta-date">11 Dec, 2018</span> <span class="meta-author">By <a href="#">Vizacafe</a></span> <span class="meta-comments"><a>Comments</a></span></p>
                        </div>
                        <div class="post-content">
                            <p>
                                New system to issue 10-year visas to investors and professionals.
                            </p>
                            <a  class="btn btn-default">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /. News section -->
    <div class="bg-primary space-small">
        <!-- call to action -->
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-7 col-sm-6 col-12">
                    <h2 class="mb10 text-white">Free online assessment</h2>
                    <%-- <p>Fusce venenatis lectus non est congue vitae malesuada neque lacinia. </p>--%>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12 text-center">
                    <a  onclick="AssesmentModel()" class="btn btn-default btn-lg mt10">Get Free Assessment</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModelForgetPass" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header" style="border-top: 2px solid #dc2e2f;">
                    <h3 class="modal-title">Forgot Password</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                <input type="email" class="form-control" id="txt_Useremail" placeholder="Email address" required="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="button" style="float: right" class="btn btn-default btn-sm" onclick="ForgetPass()">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>


    </div>

    
    
    <script>
        function SignInModel(Name, Price, Country) {
            CheckSession();
            $("#Name").val(Name);
            $("#Price").val(Price);
            $("#Country").val(Country);
        }
    </script>
    <script>
        function GetModel() {
            $("#ModelLogin").modal('hide');
            $("#ModelForgetPass").modal('show');
        }
       
    </script>
</asp:Content>
