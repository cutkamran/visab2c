﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="30days-above59yr-qatarvisa.aspx.cs" Inherits="VisaB2C._30days_above59yr_visa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>30 Days Qatar Visa</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="best Visa immigration business consulting ">
    <meta name="keywords" content="consulting ">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="1025989426784-pmfpoftgfplpendosl8fmjdmos0ke635.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="script/GoogleSignIn.js"></script>
    <script src="script/CheckSession.js"></script>
    <script src="script/Login.js"></script>
     <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- FontAwesome 4.0 CSS -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <!-- Google Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700%7cPT+Serif:400,400i,700,700i" rel="stylesheet">
    <!-- owl.carousel.min.css -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">


    <!-- fontello.css -->
    <link rel="stylesheet" type="text/css" href="css/fontello.css">
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div class="header-transparent">
        <div class="topbar-transparent">
            <!-- topbar transparent-->
            <%--  <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-none d-sm-none d-lg-block d-xl-block">
                        <p class="welcome-text">Welcome to Visacafe a immigration company </p>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="header-block">
                            <span class="header-link d-none d-xl-block d-md-block"><a href="#">Talk to Our Expert</a></span>
                            <span class="header-link">+1 800 123 4567</span>
                            <span class="header-link">
                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                            </span>
                            <span class="header-link"><a href="#" onclick="SignInModel()" class="btn btn-default btn-sm">Sign In</a></span>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
        <!-- /.topbar transparent-->
        <!-- header classic -->
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                    <a href="Default.aspx" class="logo">
                        <img src="images/logo-3.png" alt="Visacafe an Immigration and Visa Consulting "></a>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
                    <div id="navigation-transparent" class="navigation-transparent">
                        <!-- navigation -->
                        <ul>
                            <li>
                                <a href="Default.aspx">Home</a>

                            </li>

                            <li>
                                <a href="#">Visas</a>
                                <ul>
                                    <%--  <li><a href="VisaImmigrationList.aspx">Visa List Page</a></li>--%>
                                    <li><a href="VisaImmigrationSingle.aspx">Visa Single Page</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">OTB</a>
                            </li>
                            <li>
                                <a href="AboutUs.aspx">About Us </a>
                            </li>
                            <li>
                                <a href="ContactUs.aspx">Contact Us </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <h1 class="page-title">30 Days Single Entry Qatar Visa</h1>
                    <p class="page-description">(above 59 years)</p>
                </div>
            </div>
        </div>
    </div>
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">About 30 Days Single Entry Qatar Visa (above 59 years)</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page breadcrumb -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="content-area">
                        <img src="images/SLIDER/qatar.jpg" alt="" class="img-fluid mb30">
                        <h2>About 30 Days Single Entry Qatar Visa</h2>
                        <p class="lead">Aenean mauris urna, rutrum id luctus ac, sagittis a risus. Suspendisse potenti. Ut feugiat urna in lacinia pretium. Vestibulum sapien dui, tincidunt at lobortis non, pulvinar nec libero. Pellentesque sagittis euismod purus, eget lacinia tellus sagittis quis.</p>
                        <p>Ut hendrerit volutpat dictum. Sed tristique finibus felis, eu posuere lorem posuere ut. Donec et nunc dolor. Nulla tempor nec risus in bibendum. Integer vel nisl pharetra, efficitur dolor ac, lacinia ex. Etiam odio purus, fermentum a lorem at, porttitor posuere sem. </p>
                        <p>Phasellus ex lorem, sollicitudin a placerat nec, pharetra ac sapien. Maecenas venenatis, ex eu condimentum pharetra, leo risus mattis arcu, at ullamcorper mi nisi ac lacus. Fusce felis nunc, malesuada sit amet justo vitae, cursus pretium justo.</p>
                        <p>Duis malesuada, diam vel posuere efficitur, odio elit pretium velit, id tincidunt arcu odio sit amet erat. Nulla maximus nulla eget nunc gravida, tincidunt mollis libero pharetra. </p>
                        <h3>30 Days Single Entry Qatar Visa Eligibility</h3>
                        <p>Maecenas id leo efficitur, ultrices odio nec, vulputate sem. Donec porta sed arcu nec ultricies. Duis ex elit, vulputate et efficitur nec, condimentum in risus. </p>
                        <p>Fusce non mi at nisl laoreet pretium. Nulla ut elementum sapien, a pulvinar augue. Sed semper sed tellus in ultrices. Nam hendrerit elit vel urna fermentum congue. Aenean varius euismod quam sed ultrices. Duis ac magna turpis. </p>
                        <h3>30 Days Single Entry Qatar Visa Benefits</h3>
                        <ul class="listnone check-circle">
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            <li>Etiam congue libero in lacus tempor, non placerat eros gravida.</li>
                            <li>Integer sit amet metus finibus, porta ex eu, viverra dolor.</li>
                            <li>Aliquam porta massa quis eros feugiat venenatis.</li>
                        </ul>
                        <h3>How to Apply</h3>
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <!-- How it work card -->
                                <div class="card-how-it-work">
                                    <div class="card-how-it-work-body">
                                        <h2 class="number-cirle">1</h2>
                                        <p>Vivamus commodo elementum puruis auctor purus dui, in elementum erat lobortid hendrerit tortor vitae metus laoreet mas at mi felis.</p>
                                    </div>
                                    <div class="slanting-pattern-small"></div>
                                </div>
                            </div>
                            <!-- /.How it work card -->
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <!-- How it work card -->
                                <div class="card-how-it-work">
                                    <div class="card-how-it-work-body">
                                        <h2 class="number-cirle">2</h2>
                                        <p>Nulla vel tortor luctus, tempus ex convallis, maximus lib d ferm sim entum, purus in ultricies hendreto curs elerisque sem leo in diam.</p>
                                    </div>
                                    <div class="slanting-pattern-small"></div>
                                </div>
                            </div>
                            <!-- /.How it work card -->
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                <!-- How it work card -->
                                <div class="card-how-it-work">
                                    <div class="card-how-it-work-body">
                                        <h2 class="number-cirle">3</h2>
                                        <p>Maecenas id leo efficitu ltrices odio ne ulputate seonec porta sed arcu nec ultricuis ex e ulputate et efficitur nec, condimentum in risus. </p>
                                    </div>
                                    <div class="slanting-pattern-small"></div>
                                </div>
                            </div>
                            <!-- /.How it work card -->
                        </div>
                        <a onclick="SignInModel('30 Days Qatar Visa (above 59year)','6000','Qatar')" class="btn btn-default">Apply For Visa</a>
                        <input type="hidden" id="Name" />
                        <input type="hidden" id="Price" />
                        <input type="hidden" id="Country" />
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="sidebar">
                        <div class="widget widget-quote-form bg-yellow">
                            <h3 class="form-title">Free Immigration Assessment</h3>
                            <p class="form-text">Find out your options for Visa by completing a free online assessment.</p>
                            <form class="sidebar-quote-form">
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label sr-only" for="yourname">Your Name</label>
                                    <div class="">
                                        <input id="yourname" name="yourname" type="text" placeholder="Your Name" class="form-control" required="">
                                    </div>
                                </div>
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label sr-only" for="email">Email</label>
                                    <div class="">
                                        <input id="email" name="email" type="email" placeholder="Email" class="form-control" required="">
                                    </div>
                                </div>
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="control-label sr-only" for="mobileno">Mobile No</label>
                                    <div class="">
                                        <input id="mobileno" name="mobileno" type="number" placeholder="Mobile No" class="form-control" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="selectVisa" class="sr-only">Select Visa</label>
                                    <select class="form-control" id="selectVisa" name="selectVisa">
                                        <option value="Select Visa">Select Visa</option>
                                        <option value="Students Visa">Students Visa</option>
                                        <option value="Business Visa">Business Visa</option>
                                        <option value="Family Visa">Family Visa</option>
                                        <option value="Travel Visa">Travel Visa </option>
                                        <option value="Work Visa">Work Visa </option>
                                        <option value="Visitor Visa">Visitor Visa </option>
                                        <option value="Migrate Visa">Migrate Visa </option>
                                        <option value="PR Visa">PR Visa</option>
                                    </select>
                                </div>
                                <!-- Textarea -->
                                <div class="form-group">
                                    <label class="control-label sr-only" for="message">Message</label>
                                    <div class="">
                                        <textarea class="form-control" id="message" name="message" rows="4" placeholder="Message"></textarea>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-default btn-lg btn-block">Book My Free Assessment</button>
                            </form>
                        </div>
                        <div class="widget-testimonial">
                            <div class="card-testimonial card-testimonial-light">
                                <!-- card testimonial start -->
                                <div class="card-testimonial-img">
                                    <img src="images/card-testimonial-img-1.jpg" alt="" class="rounded-circle">
                                </div>
                                <div class="card-testimonial-content">
                                    <p class="card-testimonial-text">“Fusce non mi at nisl laoreet pretiumulla ut elementum sapien, a pulvinar augueed semper sed tellus in ultrices am simply dummy content hendrerit elit vel urna fermentum congue. . ”</p>
                                </div>
                                <div class="card-testimonial-info">
                                    <span class="card-testimonial-flag">
                                        <img src="images/country/canada.svg" class="flag-small"></span>
                                    <h4 class="card-testimonial-name">Dustin A. Morgan</h4>
                                    <p><small>( Canada Students Visa )</small></p>
                                </div>
                            </div>
                            <!-- /.card testimonial start -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-dark">
        <!-- Footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <%-- <h3 class="widget-title">Important Links</h3>--%>
                        <div class="box">
                            <img src="images/sslcertificate.png" width="100" height="79" alt="Secure Payment">
                            <img src="images/secure-transaction.png" width="100" height="79" alt="Secure Transaction">
                        </div>
                        <div class="box">
                            <img src="images/godaddy.gif" />
                        </div>
                        <br />
                        <div>
                            <img src="images/mastercard.png" />
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <h3 class="widget-title">Visas</h3>
                        <ul class="listnone arrow-footer">
                            <li><a href="#">30 Days Single Entry </a></li>
                            <li><a href="#">30 Days Multiple Entry </a></li>
                            <li><a href="#">90 Days Single Entry </a></li>
                            <li><a href="#">90 Days Multiple Entry </a></li>
                            <li><a href="#">14 Days Single Entry Bahrain </a></li>
                            <li><a href="#">14 Days Multiple Entry Bahrain</a></li>
                            <%--  <li><a href="#">Oman 10 Days Tourist Visa</a></li>
                            <li><a href="#">Oman 01 year Tourist Visa Multiple Entry</a></li>--%>
                        </ul>
                    </div>
                </div>
                <%--  <div class="col-xl-2 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <h3 class="widget-title">Contact Us</h3>

                        <p>
                            1800 102 4150
                            <br>
                            1800 102 4151
                        </p>
                        <p>
                            <a href="#">Schedule a Meeting</a>
                            <br>
                            <a href="#">Talk to our Expert</a>
                        </p>
                    </div>
                </div>--%>
                <div class="col-xl-2 col-lg-2 col-md-4 col-sm-2 col-6">
                    <div class="widget-footer widget-social">
                        <h3 class="widget-title">Connect</h3>
                        <ul class="listnone">
                            <li><a href="https://www.facebook.com/Vizacafe-2285529498390513"><i class="fa fa-facebook social-icon"></i>Facebook</a></li>
                            <li><a href="https://twitter.com/Viza16530470"><i class="fa fa-twitter social-icon"></i>Twitter</a></li>
                            <li><a href="https://www.instagram.com/vizacafe2080/"><i class="fa fa-instagram social-icon"></i>Instagram</a></li>
                            <li><a href="https://www.youtube.com/channel/UCJUtR8T3rX0iFDttSFZ8fbQ"><i class="fa fa-youtube social-icon"></i>Youtube</a></li>
                            <li><a href="#"><i class="fa fa-linkedin social-icon"></i>Linked In</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h3 class="widget-title">GET IMMIGRATION TIPS</h3>
                        <%--  <p>Sign up for our Newsletter and join us on the path to success.</p>--%>
                        <form method="post" action="#">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="newsletteremail">Email</label>
                                    <input type="email" class="form-control" id="newsletteremail" name="newsletteremail" placeholder="Enter Email Address">
                                </div>

                            </div>
                            <button type="submit" class="btn btn-default">Sign UP</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.footer -->
    <div class="tiny-footer-dark">
        <!-- tiny footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <p>Copyright © 2018 vizacafe.com | All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModelLogin" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">

            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="border-top: 2px solid #dc2e2f;">
                    <h3 class="modal-title">Register with us</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="bs-example" style="display: none" id="show_sucss">
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">×</a>
                            <strong>Success!</strong> Your data has been saved successfully..
                        </div>
                    </div>
                    <div class="col-md-12 row">


                        <div class="col-md-6 ">
                            <div class="col-md-12" style="text-align: center; margin-top: 16px; padding-right: 0; padding-left: 0;">
                                Continue as Guest
                      <%--<img src="images/logo-3.png" alt="" style="width: 190px; margin-top: 25px">--%>
                                <form class="form" role="form" accept-charset="UTF-8" id="login-nav" style="margin-top: 34px">
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputEmail2">Name</label>
                                        <input type="text" class="form-control" autocomplete="off" id="txt_name" placeholder="Name" required="">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                        <input type="email" class="form-control" autocomplete="off" id="txt_mail" placeholder="Email address" required="">
                                    </div>
                                    <div class="form-group" style="margin-bottom: 25px">
                                        <label class="sr-only" for="exampleInputPassword2">Phone Number</label>
                                        <input type="tel" class="form-control" autocomplete="off" id="txt_mobileno" placeholder="Phone number" required="">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-default btn-sm" style="top: -10px" onclick="UserDetail()">Continue</button>


                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="row" style="border-left: 1px solid #D5D5D5">
                                <div class="col-md-12" style="text-align: center;">
                                    Login with
                                    <br>
                                    <div class="row">
                                        <div class="col">
                                            <a href="#" onclick="fbLogin()" style="color: white; padding: 8px; border-radius: 2px; height: 33px; width: 104px; margin-right: -10px;" class="fb"><i class="fa fa-facebook"></i>Facebook</a>
                                        </div>
                                        <br />
                                        <%--  <a href="#" data-onsuccess="onSignIn" style="color: white; padding: 5px; border-radius: 2px;" class="gg"><i class="fa fa-google-plus"></i>Google</a>--%>
                                        <div class="col">
                                            <div style="margin-left: 42px; margin-top: 6px; height: 33px; width: 104px;"
                                                class="g-signin2" data-onsuccess="onSignIn" data-theme="dark">
                                            </div>
                                        </div>
                                    </div>
                                    or
                                <p style="font-size: 16px"><b>Visacafe Account</b></p>
                                    <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                            <input type="email" class="form-control" id="txt_email" placeholder="Email address" required="">
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputPassword2">Password</label>
                                            <input type="password" class="form-control" id="txt_password" placeholder="Password" required="">
                                            <div class="help-block text-right" style="cursor: pointer;"><a onclick="GetModel()">Forgot the password ?</a></div>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default btn-sm" onclick="Login()">Login</button>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox">
                                                keep me logged-in
                                            </label>
                                        </div>

                                    </form>

                                </div>

                            </div>
                        </div>

                    </div>
                    <%-- <div class="row">
                        <div class="col-md-6">
                            <img src="images/fb-login.png" onclick="fbLogin()" style="width: 100%; margin-top: 5px; cursor: pointer" />
                        </div>

                        <div class="col-md-6">
                            <br />
                            <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
                        </div>
                    </div>--%>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="ModelForgetPass" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header" style="border-top: 2px solid #dc2e2f;">
                    <h3 class="modal-title">Forgot Password</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                <input type="email" class="form-control" id="txt_Useremail" placeholder="Email address" required="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="button" style="float: right" class="btn btn-default btn-sm" onclick="ForgetPass()">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <!-- menumaker js -->
    <script src="js/menumaker.js"></script>
    <script src="js/navigation.js"></script>
    <!-- owl.carousel.min.js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom-carousel.js"></script>
    <!--Magnific-Video-Popup-->

    <script>
        function SignInModel(Name, Price, Country) {
            $("#Name").val(Name);
            $("#Price").val(Price);
            $("#Country").val(Country);
            CheckSession();
        }
    </script>
    <script>
        function GetModel() {
            $("#ModelLogin").modal('hide');
            $("#ModelForgetPass").modal('show');
        }
    </script>


    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>
