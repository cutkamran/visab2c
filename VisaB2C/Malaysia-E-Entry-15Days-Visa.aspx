﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Malaysia-E-Entry-15Days-Visa.aspx.cs" Inherits="VisaB2C.Malaysia_E_Entry_15Days_Visa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Malaysia E-Entry-15Days Visa</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="best Visa immigration business consulting ">
    <meta name="keywords" content="consulting ">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="1025989426784-pmfpoftgfplpendosl8fmjdmos0ke635.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="script/GoogleSignIn.js"></script>
    <script src="script/CheckSession.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- FontAwesome 4.0 CSS -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <!-- Google Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700%7cPT+Serif:400,400i,700,700i" rel="stylesheet">
    <!-- owl.carousel.min.css -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">

     <link href="../css/intlTelInput.css" rel="stylesheet" />
     <script src="script/Login.js"></script>
     <script src="js/jquery-2.1.3.min.js"></script>
    <!-- fontello.css -->
    <link rel="stylesheet" type="text/css" href="css/fontello.css">
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="header-transparent">
        <div class="topbar-transparent">
            <!-- topbar transparent-->
            <%--  <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-none d-sm-none d-lg-block d-xl-block">
                        <p class="welcome-text">Welcome to Visacafe a immigration company </p>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="header-block">
                            <span class="header-link d-none d-xl-block d-md-block"><a href="#">Talk to Our Expert</a></span>
                            <span class="header-link">+1 800 123 4567</span>
                            <span class="header-link">
                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                            </span>
                            <span class="header-link"><a href="#" onclick="SignInModel()" class="btn btn-default btn-sm">Sign In</a></span>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
        <!-- /.topbar transparent-->
        <!-- header classic -->
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                    <a href="Default.aspx" class="logo">
                        <img src="images/logo-3.png" alt="Visacafe an Immigration and Visa Consulting "></a>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
                    <div id="navigation-transparent" class="navigation-transparent">
                        <!-- navigation -->
                        <ul>
                            <li>
                                <a href="Default.aspx">Home</a>

                            </li>

                            <li>
                                <a href="#">Visas</a>
                                <ul>
                                    <%--  <li><a href="VisaImmigrationList.aspx">Visa List Page</a></li>--%>
                                    <li><a href="VisaImmigrationSingle.aspx">Malaysia E-Entry-15Days Visa</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">OTB</a>
                            </li>
                            <li>
                                <a href="AboutUs.aspx">About Us </a>
                            </li>
                            <li>
                                <a href="ContactUs.aspx">Contact Us </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <h1 class="page-title">Malaysia E Entry 15 Days Visa</h1>
                    <p class="page-description"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">About Malaysia E Entry 15 Days Visa</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page breadcrumb -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="content-area">
                        <img src="images/SLIDER/qatar.jpg" alt="" class="img-fluid mb30">
                        <h2>About Malaysia E Entry 15 Days Visa</h2>
                        <%-- <p class="lead">Whom is a tourist visa for?</p>--%>
                        <h3>Who can apply for eNTRI visa?</h3>
                        <p>All Indian Nationals in India and Indian expatriates residing all over the world excluding Singapore.</p>
                        <h3>How long can I stay with eNTRI visa?</h3>
                        <p>
                            An eNTRI holder is entitled to maximum of 15 days for each visit. Each application can only be used once. 
                              No extension is allowed.
                        </p>
                        <h3>How to Apply</h3>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="sidebar">
                        <div class="widget-testimonial">
                            <div class="card-testimonial card-testimonial-light">
                                <!-- card testimonial start -->
                                <div class="card-testimonial-img">
                                    <img src="images/card-testimonial-img-1.jpg" alt="" class="rounded-circle">
                                </div>
                                <div class="card-testimonial-content">
                                    <p class="card-testimonial-text">“Fusce non mi at nisl laoreet pretiumulla ut elementum sapien, a pulvinar augueed semper sed tellus in ultrices am simply dummy content hendrerit elit vel urna fermentum congue. . ”</p>
                                </div>
                                <div class="card-testimonial-info">
                                    <span class="card-testimonial-flag">
                                        <img src="images/country/canada.svg" class="flag-small"></span>
                                    <h4 class="card-testimonial-name">Dustin A. Morgan</h4>
                                    <p><small>( Malaysia Visa )</small></p>
                                </div>
                            </div>
                            <!-- /.card testimonial start -->
                        </div>
                        <a href="#" onclick="SignInModel('Malaysia E Entry 15 Days Visa','3000','Malaysia')" class="btn btn-default">Apply For Visa</a>
                        <input type="hidden" id="Name" />
                        <input type="hidden" id="Price" />
                        <input type="hidden" id="Country" />
                    </div>
                </div>
            </div>
            <%--  <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="content-area">
                        <h3>What is the validity of a tourist visa?</h3>
                        <p>Depending on your plan, tourist visas to the UAE can be issued for 14 days, 30 days or 90 days duration, single entry or multiple entry.</p>
                        <h3>Who can apply for your tourist visa?</h3>
                        <p>The UAE embassies do not issue tourist visas. In order to get a tourist visa to the UAE, you need to click here and we will apply for visa on your behalf to the official visa-issuing authorities in the UAE.</p>
                        <h3>What if someone over stay the period granted?</h3>
                        <p>Visa over stayers will have to pay AED100 fine for each day of their overstay, to be calculated from first day after the visa expiry. After 5 days over stayers will be filed as absconded, and once arrested by authorities will be jailed and will have to pay AED6000 fine plus AED100 each day over stayed and over stayer will be blacklisted from UAE will not be able to revisit the country.</p>
                        <h3>Steps</h3>
                         <ul class="listnone check-circle">
                            <li>Fill up short information and upload your documents</li>
                            <li> Your application will be reviewed and processed.</li>
                            <li>You will be notified once visa is issued, you can print e-visa & fly.</li> 
                        <h3>How to Apply</h3>

                    </div>
                </div>
            </div>--%>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">1</h2>
                            <p>Fill up short information and upload your documents</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">2</h2>
                            <p>Your application will be reviewed and processed</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">3</h2>
                            <p>You will be notified once visa is issued,you can print e-visa & fly</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
            </div>
        </div>
        <div class="space-medium pdb0">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-1 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="faq-header">
                            <h2 class="faq-title">Visa &amp; Immigration</h2>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-xl-1 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div id="accordion-2">
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingFour">
                                    <h5 class="mb0">
                                        <button class="accordion-btn collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            What are the terms and conditions for eNTRI visa?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <ul class="listnone check-circle">
                                            <li>Upon completion of registration, applicants are required to print out the eNTRI visa Note, which must be presented upon arrival in Malaysia.</li>
                                            <li>eNTRI visa facility are available from with extension from January 11th, 2019 until December 31st 2019 for All Indian nationals only excluding Singapore.</li>
                                            <li>Applicants with direct flight from India to Malaysia OR India to Malaysia via Singapore/Thailand/Brunei via the following options:</li>
                                            <li>Applicants travelling via Cruise/Ship Vessel is not allowed under eNTRI visa category. Applicant has to apply eVisa 30 days only.</li>
                                            <li>Applicants Transiting from any other country not allowed to enter under eNTRI visa.</li>
                                            <li><strong>By air</strong>
                                                <ul class="listnone check-circle">
                                                    <li>Kuala Lumpur International Airport , Sepang (KLIA & KLIA 2)</li>
                                                    <li>Penang International Airport, Pulau Pinang</li>
                                                    <li>Langkawi International Airport, Kedah</li>
                                                    <li>Melaka International Airport, Melaka</li>
                                                    <li>Senai International Airport, Johor</li>
                                                    <li>Kuching International Airport, Sarawak</li>
                                                    <li>Miri International Airport, Sarawak</li>
                                                    <li>Kota Kinabalu International Airport, Sabah</li>
                                                    <li>Labuan International Airport, Sabah</li>
                                                </ul>
                                            </li>
                                            <li><strong>By land</strong>
                                                <ul class="listnone check-circle">
                                                    <li>Sultan Iskandar Building Immigration Checkpoint, Johor</li>
                                                    <li>Sultan Abu Bakar Checkpoint, Johor</li>
                                                    <li>Padang Besar Checkpoint, Perlis</li>
                                                    <li>Bukit Kayu Hitam Checkpoint, Kedah</li>
                                                    <li>Sungai Tujuh Immigration Checkpoint, Sarawak</li>
                                                    <li>Tedungan Immigration Checkpoint, Sarawak</li>
                                                </ul>
                                            </li>
                                            <li>Applicant must return ticket directly from Malaysia to India OR Malaysia to India via Singapore/Thailand/Brunei OR Malaysia to Any Country eg: Indonesia, Australia etc.</li>
                                        </ul>



                                    </div>
                                </div>
                            </div>
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingFive">
                                    <h5 class="mb0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            What are the documents needed upon arrival at Malaysian entry checkpoints?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <p>This following documents are required to be presented upon arrival in Malaysia entry checkpoints:</p>
                                        <ul class="listnone check-circle">
                                            <li>eNTRI visa printout.</li>
                                            <li>Boarding pass</li>
                                            <li>Sufficient funds (Cash/Debit or Credit Cards / Travellers Cheque)</li>
                                            <li>Accommodation Proof</li>
                                            <li>Confirmed returned flight ticket</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingSix">
                                    <h5 class="mb-0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            How long can I stay with eNTRI visa?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <p>
                                            An eNTRI holder is entitled to maximum of 15 days for each visit. Each application can only be used once. 
                                            No extension is allowed
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-dark">
        <!-- Footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <%-- <h3 class="widget-title">Important Links</h3>--%>
                        <div class="box">
                            <img src="images/sslcertificate.png" width="100" height="79" alt="Secure Payment">
                            <img src="images/secure-transaction.png" width="100" height="79" alt="Secure Transaction">
                        </div>
                        <div class="box">
                            <img src="images/godaddy.gif" />
                        </div>
                        <br />
                        <div>
                            <img src="images/mastercard.png" />
                        </div>
                    </div>
                </div>
                  <div class="col-xl-4 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <h3 class="widget-title">Visas</h3>
                        <ul class="listnone arrow-footer">
                            <li><a href="dubai30days-singleentry-visa.aspx">30 Days Single Entry Dubai Visa</a></li>
                            <li><a href="dubai-30days-multipleentry-Visa.aspx">30 Days Multiple Entry Dubai Visa</a></li>
                            <li><a href="Dubai-90-Days-SingleEntry-visa.aspx">90 Days Single Entry Dubai Visa </a></li>
                            <li><a href="dubai-90days-multipleentry-visa.aspx">90 Days Multiple Entry Dubai Visa</a></li>
                            <li><a href="Malaysia-E-Entry-15Days-Visa.aspx">E-Entry 15 Days Malaysia Visa</a></li>
                            <li><a href="Malaysia-E-30Days-Visa.aspx">Malaysia E 30 Days Visa</a></li>
                            <%--  <li><a href="#">Oman 10 Days Tourist Visa</a></li>
                            <li><a href="#">Oman 01 year Tourist Visa Multiple Entry</a></li>--%>
                        </ul>
                    </div>
                </div>
                <%--  <div class="col-xl-2 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <h3 class="widget-title">Contact Us</h3>

                        <p>
                            1800 102 4150
                            <br>
                            1800 102 4151
                        </p>
                        <p>
                            <a href="#">Schedule a Meeting</a>
                            <br>
                            <a href="#">Talk to our Expert</a>
                        </p>
                    </div>
                </div>--%>
                <div class="col-xl-2 col-lg-2 col-md-4 col-sm-2 col-6">
                    <div class="widget-footer widget-social">
                        <h3 class="widget-title">Connect</h3>
                        <ul class="listnone">
                            <li><a href="https://www.facebook.com/Vizacafe-2285529498390513"><i class="fa fa-facebook social-icon"></i>Facebook</a></li>
                            <li><a href="https://twitter.com/Viza16530470"><i class="fa fa-twitter social-icon"></i>Twitter</a></li>
                            <li><a href="https://www.instagram.com/vizacafe2080/"><i class="fa fa-instagram social-icon"></i>Instagram</a></li>
                            <li><a href="https://www.youtube.com/channel/UCJUtR8T3rX0iFDttSFZ8fbQ"><i class="fa fa-youtube social-icon"></i>Youtube</a></li>
                            <li><a href="#"><i class="fa fa-linkedin social-icon"></i>Linked In</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h3 class="widget-title">GET IMMIGRATION TIPS</h3>
                        <%--  <p>Sign up for our Newsletter and join us on the path to success.</p>--%>
                        <form method="post" action="#">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="newsletteremail">Email</label>
                                    <input type="email" class="form-control" id="newsletteremail" name="newsletteremail" placeholder="Enter Email Address">
                                </div>

                            </div>
                            <button type="submit" class="btn btn-default">Sign UP</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.footer -->
    <div class="tiny-footer-dark">
        <!-- tiny footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <p>Copyright © 2018 vizacafe.com | All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>

     <div class="modal fade" id="ModelLogin" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">

            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="border-top: 2px solid #dc2e2f;">
                    <h3 class="modal-title">Register with us</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="bs-example" style="display: none" id="show_sucss">
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">×</a>
                            <strong>Success!</strong> Your data has been saved successfully..
                        </div>
                    </div>
                    <div class="col-md-12 row">


                        <div class="col-md-6 ">
                            <div class="col-md-12" style="text-align: center; margin-top: 16px; padding-right: 0; padding-left: 0;">
                                Continue as Guest
                      <%--<img src="images/logo-3.png" alt="" style="width: 190px; margin-top: 25px">--%>
                                <form class="form" role="form" accept-charset="UTF-8" id="login-nav" style="margin-top: 34px">
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputEmail2">Name</label>
                                        <input type="text" class="form-control" autocomplete="off" id="txt_name" placeholder="Name" required="">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                        <input type="email" class="form-control" autocomplete="off" id="txt_mail" placeholder="Email address" required="">
                                    </div>
                                    <div class="form-group" style="margin-bottom: 25px">
                                        <label class="sr-only" for="exampleInputPassword2">Phone Number</label>
                                        <input type="tel" class="form-control" autocomplete="off" id="txt_mobileno" placeholder="Phone number" required="">
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-default btn-sm" style="top: -10px" onclick="UserDetail()">Continue</button>


                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="row" style="border-left: 1px solid #D5D5D5">
                                <div class="col-md-12" style="text-align: center;">
                                    Login with
                                    <br>
                                    <div class="row">
                                        <div class="col">
                                            <a href="#" onclick="fbLogin()" style="color: white; padding: 8px; border-radius: 2px;height:33px; width:104px; margin-right: -10px; " class="fb"><i class="fa fa-facebook"></i>Facebook</a>
                                        </div>
                                        <br />
                                        <%--  <a href="#" data-onsuccess="onSignIn" style="color: white; padding: 5px; border-radius: 2px;" class="gg"><i class="fa fa-google-plus"></i>Google</a>--%>
                                        <div class="col">
                                            <div style="margin-left: 42px;margin-top:6px; height: 33px; width: 104px;"
                                                class="g-signin2" data-onsuccess="onSignIn" data-theme="dark">
                                            </div>
                                        </div>
                                    </div>
                                    or
                                <p style="font-size: 16px"><b>Vizacafe Account</b></p>
                                    <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                            <input type="email" class="form-control" id="txt_email" placeholder="Email address" required="">
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="exampleInputPassword2">Password</label>
                                            <input type="password" class="form-control" id="txt_password" placeholder="Password" required="">
                                            <div class="help-block text-right" style="cursor: pointer;"><a onclick="GetModel()">Forgot the password ?</a></div>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default btn-sm" onclick="Login()">Login</button>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox">
                                                keep me logged-in
                                            </label>
                                        </div>

                                    </form>

                                </div>

                            </div>
                        </div>

                    </div>
                    <%-- <div class="row">
                        <div class="col-md-6">
                            <img src="images/fb-login.png" onclick="fbLogin()" style="width: 100%; margin-top: 5px; cursor: pointer" />
                        </div>

                        <div class="col-md-6">
                            <br />
                            <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
                        </div>
                    </div>--%>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

     <div class="modal fade" id="ModelForgetPass" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header" style="border-top: 2px solid #dc2e2f;">
                    <h3 class="modal-title">Forgot Password</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12 row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                <input type="email" class="form-control" id="txt_Useremail" placeholder="Email address" required="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="button" style="float: right" class="btn btn-default btn-sm" onclick="ForgetPass()">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>


    </div>
     <script>
        function SignInModel(Name, Price, Country) {
            $("#Name").val(Name);
            $("#Price").val(Price);
            $("#Country").val(Country);
            CheckSession();
        }
    </script>
    <script>
        function GetModel() {
            $("#ModelLogin").modal('hide');
            $("#ModelForgetPass").modal('show');
        }
    </script>
      <script>
        $(function () {
            $("#txt_mobileno").intlTelInput({
            });
        })
    </script>
     <script src="../js/intlTelInput.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <%-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--%>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <!-- menumaker js -->
    <script src="js/menumaker.js"></script>
    <script src="js/navigation.js"></script>
    <!-- owl.carousel.min.js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom-carousel.js"></script>
    <!--Magnific-Video-Popup-->
</body>
</html>
