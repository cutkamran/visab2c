﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="VisaImmigrationList.aspx.cs" Inherits="VisaB2C.VisaImmigrationList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                             <br /><br /><br />  <br />
     <!-- /.header classic -->
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <h1 class="page-title">Visa Immigration List </h1>
                    <p class="page-description">Visa Immigration List page to present your Visa service with thumbnail image, title and description with read more link.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Immigration Visa List</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page breadcrumb -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12 ">
                    <div class="Visa-card">
                        <div class="Visa-card-img">
                            <a href="#"> <img src="images/Visa-img-3.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="Visa-card-content">
                            <h3 class="Visa-card-title"><a href="#" class="title">Job Seeker Visa</a></h3>
                            <p>Duis id quam semper, eleifend lorem in, imperdiet mauris simple estibulum imple et efficitur nunc.</p>
                            <a href="#" class="btn-link-primary">Read More</a>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                    <div class="Visa-card">
                        <div class="Visa-card-img">
                            <a href="#"> <img src="images/Visa-img-1.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="Visa-card-content">
                            <h3 class="Visa-card-title"><a href="#" class="title">Student Visa</a></h3>
                            <p>Studying In The canada The generom repetition jected humour, or non-characteristic words etc.</p>
                            <a href="#" class="btn-link-primary">Read More</a>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                    <div class="Visa-card">
                        <div class="Visa-card-img">
                            <a href="#"> <img src="images/Visa-img-2.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="Visa-card-content">
                            <h3 class="Visa-card-title"><a href="#" class="title">Business Visa</a></h3>
                            <p>Quam semper simple dummy eleifend lorem in imperdiet mauris estibulum et efficitur lorem ipsum.</p>
                            <a href="#" class="btn-link-primary">Read More</a>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                    <div class="Visa-card">
                        <div class="Visa-card-img">
                            <a href="#"> <img src="images/Visa-img-4.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="Visa-card-content">
                            <h3 class="Visa-card-title"><a href="#" class="title">Tourist & Visitor Visa</a></h3>
                            <p>Suspendisse lobortis eros ac blan mi aliquet justo sit amet aliquet nunc simple dummy content risus a felis</p>
                            <a href="#" class="btn-link-primary">Read More</a>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12 clearfix">
                    <div class="Visa-card">
                        <div class="Visa-card-img">
                            <a href="#"> <img src="images/Visa-img-5.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="Visa-card-content">
                            <h3 class="Visa-card-title"><a href="#" class="title">Family Visa</a></h3>
                            <p>Duis id quam semper, eleifend lorem in, imperdiet mauris simple estibulum imple et efficitur nunc.</p>
                            <a href="#" class="btn-link-primary">Read More</a>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                    <div class="Visa-card">
                        <div class="Visa-card-img">
                            <a href="#"> <img src="images/Visa-img-6.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="Visa-card-content">
                            <h3 class="Visa-card-title"><a href="#" class="title">Travel Visa</a></h3>
                            <p>Studying In The canada The generom repetition jected humour, or non-characteristic words etc.</p>
                            <a href="#" class="btn-link-primary">Read More</a>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                    <div class="Visa-card">
                        <div class="Visa-card-img">
                            <a href="#"> <img src="images/Visa-img-7.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="Visa-card-content">
                            <h3 class="Visa-card-title"><a href="#" class="title">Migrate Visa</a></h3>
                            <p>Quam semper simple dummy eleifend lorem in imperdiet mauris estibulum et efficitur lorem ipsum.</p>
                            <a href="#" class="btn-link-primary">Read More</a>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-3 col-12">
                    <div class="Visa-card">
                        <div class="Visa-card-img">
                            <a href="#"> <img src="images/Visa-img-8.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="Visa-card-content">
                            <h3 class="Visa-card-title"><a href="#" class="title">PR Visa</a></h3>
                            <p>Suspendisse lobortis eros ac blan mi aliquet justo sit amet aliquet nunc simple dummy content risus a felis</p>
                            <a href="#" class="btn-link-primary">Read More</a>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
