﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VisaB2C.Datalayer;
using VisaB2C.dbml;

namespace VisaB2C.handler
{
    /// <summary>
    /// Summary description for LoginDetail
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LoginDetail : System.Web.Services.WebService
    {
        string json = "";

        [WebMethod(EnableSession = true)]
        public string AddUserDetail(string Name, string nMobile, string email)
        {
            json = LoginUser.AddUser(Name, nMobile, email);
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateUser(string name, string useremail, string Contact)
        {
            json = LoginUser.UpdateUserDetail(name, useremail, Contact);
            return json;
        }



        [WebMethod(EnableSession = true)]
        public string UserLogin(string email, string password)
        {
            json = LoginUser.UserLoginDetail(email, password);
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string ForgetPass(string email)
        {
            json = LoginUser.ForgetPass(email);
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CheckSession()
        {
            json = LoginUser.CheckUserSession();
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetProfile()
        {
            json = LoginUser.GetUserProfile();
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetUploadedDocs()
        {
            json = LoginUser.GetUploadedDocs();
            return json;

        }

        [WebMethod(EnableSession = true)]
        public string CheckGoogleSession(string Id, string name, string Email)
        {
            json = LoginUser.GoogleSession(Id, name, Email);
            return json;
        }



        [WebMethod(EnableSession = true)]
        public string Logout()
        {

            HttpContext.Current.Session["LoginUser"] = null;
            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            return json;
        }

    }
}
