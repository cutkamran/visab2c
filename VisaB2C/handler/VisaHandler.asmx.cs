﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using VisaB2C.Datalayer;
using VisaB2C.dbml;

namespace VisaB2C.handler
{
    /// <summary>
    /// Summary description for VisaHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VisaHandler : System.Web.Services.WebService
    {
        JavaScriptSerializer objSerlizer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random generator = new Random();
            rndnumber = generator.Next(9999, 999999999);
            return rndnumber;
        }

        [WebMethod(true)]
        public string AddVisa(List<tbl_VisaDetail> arrVISA)
        {
            string jsonString = "";
            try
            {
                UserInfo ListSession = (UserInfo)HttpContext.Current.Session["LoginUser"];
                List<string> RefList = new List<string>();
                if (ListSession != null)
                {
                    arrVISA.ForEach(d => d.UserId = ListSession.Sid);
                    foreach (var Visa in arrVISA)
                    {
                        Visa.Vcode = "";
                        Visa.Vcode = "CUT-" + GenerateRandomNumber();
                        RefList.Add(Visa.Vcode);
                    }
                    using (var DB = new helperDataContext())
                    {
                        DB.tbl_VisaDetails.InsertAllOnSubmit(arrVISA);
                        DB.SubmitChanges();
                    }

                    jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 1, RefList = RefList });
                }
                else
                {
                    jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
                }
            }
            catch (Exception ex)
            {
                jsonString = objSerlizer.Serialize(new { Session = 1, retCode = 0 });
            }
            //VisaManager.AddVisaDetails(sProcessing, dteArrival, dteDeparting, sType, sFirst, sMiddle, sLast, sFather, sMother, sHusband, sLanguage, sGender, sMarital, sNationality, sBirth, sBirthPlace, sBirthCountry, sReligion, sProfession, sPassport, sIssuing, sIssueDate, sExpirationDate, sAddress1, sAddress2, sCity, sCountry, sTelephone, sVisa, sArriAirLine, sArriFlight, sArrivalFrom, sDeptAirLine, sDeptFlight, sDeptFrom, UserId, VisaFee, OtherFee, UrgentFee, ServiceTax, TotalAmount, sVisaCountry, AppDate, Franchisee);
            return jsonString;
        }

        [WebMethod(true)]
        public string GetVisa()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            string json = "";
            using (var DB = new helperDataContext())
            {


                try
                {
                    UserInfo objUserInfo = (UserInfo)HttpContext.Current.Session["LoginUser"];

                    var List = (from obj in DB.tbl_VisaDetails
                                where obj.UserId == objUserInfo.Sid
                                select new
                                {
                                    FirstName = obj.FirstName,
                                    LastName = obj.LastName,
                                    IeService = obj.IeService,
                                    Vcode = obj.Vcode,
                                    PassportNo = obj.PassportNo,
                                    ExpDate = obj.ExpDate,
                                    ArrivalDate = obj.ArrivalDate,
                                    DepartingDate = obj.DepartingDate,
                                    VisaFee = obj.VisaFee,
                                    TotalAmount = obj.TotalAmount
                                }).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                catch (Exception ex)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            return json;
        }

        //[WebMethod(true)] 
        //public string Search()
        //{

        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    string json = "";
        //    using (var DB = new helperDataContext())
        //    {
        //        try
        //        {
        //            UserInfo objUserInfo = (UserInfo)HttpContext.Current.Session["LoginUser"];
        //            var List = (from obj in DB.tbl_VisaDetails
        //                        where obj.sid == objUserInfo.Sid
        //                        select new
        //                        {
        //                            FirstName = obj.FirstName,
        //                            LastName = obj.LastName,
        //                            IeService = obj.IeService,
        //                            Vcode = obj.Vcode,
        //                            PassportNo = obj.PassportNo,
        //                            ExpDate = obj.ExpDate,
        //                            ArrivalDate = obj.ArrivalDate,
        //                            DepartingDate = obj.DepartingDate,
        //                            VisaFee = obj.VisaFee,
        //                            TotalAmount = obj.TotalAmount
        //                        }).ToList();

        //            json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
        //        }
        //        catch (Exception ex)
        //        {

        //            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //        }

        //    }

        //    return json;

        //}

    }
}
