﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using VisaB2C.Datalayer;

namespace VisaB2C.handler
{
    /// <summary>
    /// Summary description for Contacthandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Contacthandler : System.Web.Services.WebService
    {

        [WebMethod(true)]
        public bool EmailSending(string userName, string nMobile, string email, string subject, string message)
        {
            //string html = emailmanager.SendMail(userName, nMobile, email, subject, message);
            try
            {
                bool response = emailmanager.SendMail(userName, nMobile, email, subject, message);
                return response;
            }
            catch
            {
                return false;
            }
        }

        [WebMethod(true)]
        public  bool AssesmentEmail(string assesmentName,string assesemail,string mobileno,string visatype,string assesmessage)
        {
            try
            {
                bool responce = emailmanager.SendAssesmentMail(assesmentName, assesemail, mobileno, visatype, assesmessage);
                return responce;
            }
            catch (Exception)
            {

                return false;
            }


        }

    }
}
