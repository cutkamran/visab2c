﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace VisaB2C.handler
{
    /// <summary>
    /// Summary description for UploadVisaDocumnet
    /// </summary>
    public class UploadVisaDocumnet : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                List<string> RefList = new List<string>();

                string FileNo = System.Convert.ToString(context.Request.QueryString["REfList"]);
                RefList = FileNo.Split(',').ToList();

                List<string> List = new List<string>();
                HttpFileCollection files = context.Request.Files;

                foreach (var Rafrence in RefList)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        List.Add(Rafrence + "_" + i);
                    }
                }

                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string myFilePath = file.FileName;
                    string ext = Path.GetExtension(myFilePath);
                    //if (ext == ".jpg" || ext == ".jpeg" || ext == ".png")
                    //{
                        string FileName = List[i]+".jpg";
                        FileName = Path.Combine(context.Server.MapPath("~/VisaDocument/"), FileName);
                        //string fname = context.Server.MapPath("~/OTBDocument/" + file.FileName);
                        file.SaveAs(FileName);
                        string FN = FileName.Replace(myFilePath, "");
                    // }
                    //else
                    //{
                    //    context.Response.Write("Only PNG/jpg file is allowed");

                    //}
                }
                context.Response.Write("Return true");
            }
            else
            {
                context.Response.Write("Return false");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}