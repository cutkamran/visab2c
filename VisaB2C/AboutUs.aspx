﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AboutUs.aspx.cs" Inherits="VisaB2C.AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <br />
    <br />
    <br />
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">About us</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page breadcrumb -->
    <div class="content pdb0">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <h2>About Us</h2>
                    <p class="lead">Vizacafe surrenders speed visa administrations to help voyagers from India to visit Dubai or the UAE. We make a visa application method simple with our calm administrations and thorough agendas, taking out any nervousness you may have about your Dubai visit. This enables you to get your Dubai visa without prior warning for a truly sensible rate. </p>
                    <p>Specialists in preparing visas for recreation and corporate explorers, our visa advisers can help you with an assortment of Dubai visas, including different section visas and express visas. Situated in Pune, India and with solid associations and tasks in the UAE too, you can rest guaranteed that we're Surronded  with all assets to promise you a safe and expert administration for your Dubai travel. </p>
                    <p>
                       In this way, in a joint effort with our group of visa specialists, you'll find applying for a Dubai visa a very smooth and quick procedure. 
                    </p>
                    <%--<p class="text-default">Call: +1 800-1234-5678</p>--%>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="image-block">
                        <img src="images/about-us-fancy-img-2.png" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="space-medium pdb0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="feature-blurb mb20">
                            <div class="feature-content">
                                <h3 class="feature-title">Our Mision</h3>
                                <p>To give fantastic visa administrations at financially savvy rates.</p>
                                <p>
                                    To speak to our customers' differed visa needs morally and ardently. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="feature-blurb">
                            <div class="feature-content">
                                <h3 class="feature-title">Our Mision</h3>
                                <p>To be responsive by giving our customers individual reports on their visa procedure through and through, subsequently dispensing with all speculation works out of acquiring a Dubai visa.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-medium">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  te">
                        <h3>Here's the reason Dubai visa .in is the ideal decision as your Dubai visa expert :</h3>
                        <h4>Experience:</h4>
                        <p>Our visa prosses  have an abundance of involvement in the hotel of utilization for a wide range of Dubai visas. This, thus, empowers us to control you in the best way, guaranteeing the endorsement and on-time conveyance of your Dubai visa.</p>
                          <h4>Administration centred: </h4>
                        <p>A focused on conveying you a viable, proficient visa administration. Notwithstanding how straightforward or muddled your visa application is, we guarantee you of most extreme straightforwardness and respectability all through the procedure. </p>                     
                    <h4>Estimating: </h4>
                        <p>Our costs are exceptionally aggressive and come with no concealed expense. we charge from you just what you see on our site.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 nopadding">
                    <div class="">
                        <img src="images/office-img-1.jpg" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 nopadding">
                    <div class="">
                        <img src="images/office-img-2.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <div class="space-medium bg-light">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="section-title text-center">
                            <!-- section title start-->
                            <h2>Why Visacafe ?</h2>
                            <p>Since our founding, our primary goal has been to provide immigration in all over country and universities. Our impact is speak louder than our word.</p>
                        </div>
                        
                    </div>
                </div>
                <div class="counter-section pdb0">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                            <div class="counter-block text-center">
                                <h2 class="counter-title text-default">10000+</h2>
                                <p class="counter-text">Clients</p>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                            <div class="counter-block text-center">
                                <h2 class="counter-title text-secondary">800+</h2>
                                <p class="counter-text"> Visa Per Month</p>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                            <div class="counter-block text-center">
                                <h2 class="counter-title text-warning">40+</h2>
                                <p class="counter-text">Visa types</p>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                            <div class="counter-block text-center">
                                <h2 class="counter-title text-default">6+</h2>
                                <p class="counter-text">Country</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

</asp:Content>
