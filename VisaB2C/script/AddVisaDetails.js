﻿
var VISA = new Array();
var VISADoc = new FormData();

//var Name, Middle, last;
//var PassNO, ExpDt, Nationality;
//var Passportfile, Photofile, Idfile;

$(document).ready(function () {

    AddMore();
    MakeSummary();
});

function Validate_VisaRegisteration() {
    debugger;

    var arrPax = $(".More");

    $(arrPax).each(function (index, ndPax) {

        var sFirst = $($(ndPax).find(".First")).val();
        if (sFirst == "") {
            $($(ndPax).find(".First")).focus();
            $($(ndPax).find(".First")).css("display", "");
            bvalid = false;
        }

        //var Middle = $($(ndPax).find(".Middle")).val();
        //if (Middle == "") {
        //    $($(ndPax).find(".Middle")).focus();
        //    $($(ndPax).find(".Middle")).css("display", "");
        //    bvalid = false;
        //}

        var Last = $($(ndPax).find(".Last")).val();
        if (Last == "") {
            $($(ndPax).find(".Last")).focus();
            $($(ndPax).find(".Last")).css("display", "");
            bvalid = false;
        }

        var Passport = $($(ndPax).find(".Passport")).val();
        if (Passport == "") {
            $($(ndPax).find(".Passport")).focus();
            $($(ndPax).find(".Passport")).css("display", "");
            bvalid = false;
        }

        var ExpDate = $($(ndPax).find(".ED").find('input:text')).val();
        if (ExpDate == "") {
            $($(ndPax).find(".ED")).focus();
            $($(ndPax).find(".EDLabel")).css("display", "");
            bvalid = false;
        }

        var Nationality = $($(ndPax).find(".Nationality")).val();
        if (Nationality == "--Salect Visa Type--") {
            $($(ndPax).find(".Nationality")).focus();
            $($(ndPax).find(".Nationality")).css("display", "");
            bvalid = false;
        }
        //$("#lbl_selNationality").text('');
        if ($($(ndPax).find(".Nationality")).val() == "-") {
            $($(ndPax).find(".Nationality")).focus();
            $($(ndPax).find(".Nationality")).css("display", "");
            bvalid = false;
        }

        if (Nationality == "205") {
            var Pancard = $($(ndPax).find(".Pancard").find('input:text')).val();
            if (Pancard == "") {
                $($(ndPax).find(".Pancard")).focus();
                $($(ndPax).find(".Pancard")).css("display", "");
                bvalid = false;
            }
        }

        var ArrivalDate = $($(ndPax).find(".ArrivalDate").find('input:text')).val();
        if (ArrivalDate == "") {
            $($(ndPax).find(".ArrivalDate")).focus();
            $($(ndPax).find(".ArrivalDateLabel")).css("display", "");
            bvalid = false;
        }

        var DepartureDate = $($(ndPax).find(".DepartureDate").find('input:text')).val();
        if (DepartureDate == "") {
            $($(ndPax).find(".DepartureDate")).focus();
            $($(ndPax).find(".DepartureDateLabel")).css("display", "");
            bvalid = false;
        }

        var Photofile = $($(ndPax).find(".Photofile")).val();
        if (Photofile == "") {
            $($(ndPax).find(".Photofile")).focus();
            $($(ndPax).find(".Photofile")).css("display", "");
            bvalid = false;
        }

        var Passportfile = $($(ndPax).find(".Passportfile")).val();
        if (Passportfile == "") {
            $($(ndPax).find(".Passportfile")).focus();
            $($(ndPax).find(".Passportfile")).css("display", "");
            bvalid = false;
        }

        var Idfile = $($(ndPax).find(".Idfile")).val();
        if (Idfile == "") {
            $($(ndPax).find(".Idfile")).focus();
            $($(ndPax).find(".Idfile")).css("display", "");
            bvalid = false;
        }

    });

    var Name = getParameterByName('Name').replace("%20", " ");
    if (Name == "") {
        bvalid = false;
    }
    var Price = getParameterByName('Price').replace("%20", " ");
    if (Price == "") {
        bvalid = false;
    }
    var Country = getParameterByName('Country').replace("%20", " ");
    if (Country == "") {
        bvalid = false;
    }

    var TotalPrice = $("#TotalPrice").text();
    if (TotalPrice == "") {
        bvalid = false;
    }

    return bvalid;
}

function CheckValidation(id) {
    var arrPax = $(".More");
    $(arrPax).each(function (index, ndPax) {
        // $($(ndPax).find(".First")).val();

        var Value = $($(ndPax).find("#" + id)).val();
        //$("#" + id).val();
        if (Value == "") {
            // $($(ndPax).find("#" + id)).focus();
            $($(ndPax).find("#lbl_" + id)).css("display", "");

            // $("#" + id).focus();
            // $("#lbl_" + id).css("display", "");
            //return false;
        }
        else if (Value == "-") {
            // $($(ndPax).find("#" + id)).focus();
            $($(ndPax).find("#lbl_" + id)).css("display", "");

            //$("#" + id).focus();
            //$("#lbl_" + id).css("display", "");
            //return false;
        }
        if (Value != "") {
            $($(ndPax).find("#lbl_" + id)).css("display", "none");
            // $("#lbl_" + id).css("display", "none");
        }
    });
}

function process(date) {
    debugger;
    var parts = date.split("-");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function CheckSessionOnPage() {
    debugger;
    $.ajax({
        url: "../handler/LoginDetail.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode != 1 || result.retCode == 0) {
                alert('Your session expire. Please login again.');
                window.location = "../Default.aspx";
            }
            else {
                SaveVisaDetails();
            }
        },
        error: function ()
        {
            alert('Error occured while authenticating user!');
            window.location.href = "../Default.aspx";
        }
    });
}

function SaveVisaDetails() {
    bvalid = true;
    //bvalid = Validate_VisaRegisteration();

    //if (bvalid == true) 
    if ($("#form1").validationEngine('validate')) {

        VISA = new Array();
        var arrPax = $(".More");

        var Service; var UrgentFee;
        if (NormalCheck.checked) {
            Service = 1;
            UrgentFee = "0.00"
        }
        else {
            Service = 2;
            UrgentFee = "500";
        }

        var VisaCountry = getParameterByName('Country').replace("%20", " ");
        var Price = getParameterByName('Price').replace("%20", " ");
        var TotalAmount = parseFloat(UrgentFee) + parseFloat(Price);

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = dd + '-' + mm + '-' + yyyy;


        $(arrPax).each(function (index, ndPax) {

            var Nationality = $($(ndPax).find(".Nationality")).val();
            var Pancard;
            if (Nationality == "205")
                Pancard = $($(ndPax).find(".Pancard")).val();
            else
                Pancard = "";

            VISA.push({
                FirstName: $($(ndPax).find(".First")).val(),

                MiddleName: $($(ndPax).find(".Middle")).val(),

                LastName: $($(ndPax).find(".Last")).val(),

                PassportNO: $($(ndPax).find(".Passport")).val(),

                ExpDate: $($(ndPax).find(".ED")).val(),

                ArrivalDate: $($(ndPax).find(".ArrivalDate")).val(),

                DepartingDate: $($(ndPax).find(".DepartureDate")).val(),

                IeService: getParameterByName('Name').replace("%20", " "),

                Country: $($(ndPax).find(".Nationality")).val(),
                BirthCountry: $($(ndPax).find(".Nationality")).val(),
                PresentNattionality: $($(ndPax).find(".Nationality")).val(),

                ZIPCode: Pancard,

                VisaCountry: VisaCountry,
                VisaFee: Price,
                OtherFee: "",

                UrgentFee: UrgentFee,
                ServiceTax: "0",
                TotalAmount: TotalAmount,

                Members: "1",
                Sponser: "Not Assign",
                ServiceType: "87",
                Processing: Service,
                PassportType: "3",
                AdminStatus: "Under Process",
                AppliedDate: today,
            })

            var fileUpload = $($(ndPax).find(".Photofile")).get(0);
            var files = fileUpload.files;
            FNIN = files[0].name;
            exIn = FNIN.split('.')
            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                VISADoc.append(files[i].name, files[i]);
            }

            var fileUpload = $($(ndPax).find(".Passportfile")).get(0);
            var files = fileUpload.files;
            FNIN = files[0].name;
            exIn = FNIN.split('.')
            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                VISADoc.append(files[i].name, files[i]);
            }

            var fileUpload = $($(ndPax).find(".Idfile")).get(0);
            var files = fileUpload.files;
            FNIN = files[0].name;
            exIn = FNIN.split('.')
            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                VISADoc.append(files[i].name, files[i]);
            }
            //VISADoc.push({
            //    PassportFile: $($(ndPax).find(".PassportFile")).val(),
            //    Photofile: $($(ndPax).find(".Photofile")).val(),
            //    Idfile: $($(ndPax).find(".Idfile")).val(),
            //})

        })

        var dataToPass =
            {
                arrVISA: VISA
            }
        $.ajax({
            type: "POST",
            url: "../handler/VisaHandler.asmx/AddVisa",
            data: JSON.stringify(dataToPass),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    var List = result.RefList;
                    UploadDocument(List);
                    //setTimeout(function myfunction() {
                    //    alert("Visa details submitted.");
                    //    window.location.href = "Default.aspx";
                    //}, 5000)

                    //  Ok("Visa submit successfully,Please  Upload Images", "Location", [sVisa])
                }
                if (result.retCode == 0)
                {
                    alert("An error occured while submitting visa details.");
                }
            },
            error: function () {
                alert("An error occured while submitting visa details.");
            }
        });
    }
}

function UploadDocument(List) {
    debugger;
    $.ajax({
        url: "../handler/UploadVisaDocumnet.ashx?REfList=" + List + "",
        type: "POST",
        data: VISADoc,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result == "Return true") {
                alert("Visa details submitted.");
                window.location.href = "../Default.aspx";
            }
            else {
                alert("An error occured while uploading visa document.Please contact administrator.");
            }
        }
    });
}

////////////// ** Add and Delete ** ///////////////

function AddMore() {
    var Div = "";



    //Div += '<div class="">'
    Div += '<div class="More md-card">'
    Div += '<div class="md-card-content">'
    Div += '<h4 class="heading_a"> Passenger Details</h4><hr/> ';
    Div += '<div class="uk-grid" data-uk-grid-margin>'

    ////////////* Zero Row *///////
    Div += ' <div class="uk-form-row" >'
    Div += ' <div class="uk-grid" data-uk-grid-margin>'

    Div += '       <div class="uk-width-medium-1-3" style="width:50%">'
    Div += '        <label>First Name  </label>'
    Div += '       <input type="text" autocomplete="off" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtFirst"  class="md-input validate[required] First"  />'
    Div += '       </div>'

    Div += '       <div class="uk-width-medium-1-3" style="width:50%">'
    Div += '        <label>Last Name  </label>'
    Div += '        <input type="text" autocomplete="off" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtLast"  class="md-input validate[required] Last"  />'
    Div += '       </div>'

    Div += '   </div>'
    Div += '   </div>'
    ////////////* End Zero Row *///////

    //Div += '<br />'
    //Div += '<h3>Passport Details'
    //Div += '</h3>'
    //Div += '<hr />'

    ////////////* First Row *///////
    Div += ' <div class="uk-form-row" >'
    Div += ' <div class="uk-grid" data-uk-grid-margin>'

    Div += '       <div class="uk-width-medium-1-3" >'
    Div += '        <label>Passport No</label>'
    Div += '           <input type="text"  autocomplete="off" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtPassport"  class="md-input  validate[required] Passport"  />'
    Div += '        </div>'

    Div += '       <div class="uk-width-medium-1-3">'
    Div += '           <div class="uk-input-group">'
    Div += '             <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>'
    Div += '               <label for="txtED">Expiry Date</label>'
    //  Div += '             <input type="text" autocomplete="off" id="txtED" placeholder="Expiry Date" class="md-input label-fixed validate[required] ED" style="cursor: pointer" onchange="CheckEDDate()" />'
    Div += '             <input type="text" autocomplete="off" data-uk-datepicker="{format:"DD-MM-YYYY"}" id="txtED"  class="md-input validate[required] ED" style="cursor: pointer" onchange="CheckEDDate()" />'
    Div += '          </div>'
    Div += '        </div>'

    Div += '       <div class="uk-width-medium-1-3">'
    Div += '           <div class="uk-input-group">'
   //  Div += '           <label>Nationality</label>'
    Div += '           <select id="selNationality" class="md-input label-fixed validate[required] Nationality" data-uk-tooltip="{pos:"top"}" title="Select with tooltip" onchange="CheckNationality()" >'
    Div += '          <option value="-" selected="selected">--Select Nationality --</option>'

    Div += '                                                       <option value="209">AFGHANISTAN</option>  '
    Div += '                                                       <option value="473">ALBANIA</option>  '
    Div += '                                                       <option value="131">ALGERIA</option>  '
    Div += '                                                       <option value="445">ANDORRA</option>  '
    Div += '                                                       <option value="305">ANGOLA</option>  '
    Div += '                                                       <option value="663">ANTIGUA &amp; BARBUDA</option>  '
    Div += '                                                       <option value="605">ARGENTINA</option>  '
    Div += '                                                       <option value="263">ARMENIA</option>  '
    Div += '                                                       <option value="701">AUSTRALIA</option>  '
    Div += '                                                       <option value="417">AUSTRIA</option>  '
    Div += '                                                       <option value="265">AZERBAIJAN</option>  '
    Div += '                                                       <option value="619">BAHAMAS</option>  '
    Div += '                                                       <option value="107">BAHRAIN</option>  '
    Div += '                                                       <option value="207">BANGLADESH</option>  '
    Div += '                                                       <option value="657">BARBADOS</option>  '
    Div += '                                                       <option value="411">BELGIUM</option>  '
    Div += '                                                       <option value="651">BELIZE</option>  '
    Div += '                                                       <option value="369">BENIN</option>  '
    Div += '                                                       <option value="634">BERMUDA</option>  '
    Div += '                                                       <option value="211">BHUTAN</option>  '
    Div += '                                                       <option value="609">BOLIVIA</option>  '
    Div += '                                                       <option value="463">BOSNIA AND HERZEG</option>  '
    Div += '                                                       <option value="375">BOTSWANA</option>  '
    Div += '                                                       <option value="603">BRAZIL</option>  '
    Div += '                                                       <option value="401">BRITAIN</option>  '
    Div += '                                                       <option value="253">BRUNEI</option>  '
    Div += '                                                       <option value="465">BULGARIA</option>  '
    Div += '                                                       <option value="351">BURKINA FASO</option>  '
    Div += '                                                       <option value="307">BURUNDI</option>  '
    Div += '                                                       <option value="393">CABO VERDE</option>  '
    Div += '                                                       <option value="215">CAMBODIA</option>  '
    Div += '                                                       <option value="309">CAMEROON</option>  '
    Div += '                                                       <option value="501">CANADA</option>  '
    Div += '                                                       <option value="363">CENTRAL AFRICA REP</option>  '
    Div += '                                                       <option value="311">CHAD</option>  '
    Div += '                                                       <option value="607">CHILE</option>  '
    Div += '                                                       <option value="219">CHINA</option>  '
    Div += '                                                       <option value="611">COLOMBIA</option>  '
    Div += '                                                       <option value="642">COMONWEALTH DOMINICA</option>  '
    Div += '                                                       <option value="301">COMOROS</option>  '
    Div += '                                                       <option value="313">CONGO Republic</option>  '
    Div += '                                                       <option value="623">COSTARICA</option>  '
    Div += '                                                       <option value="453">CROATIA</option>  '
    Div += '                                                       <option value="621">CUBA</option>  '
    Div += '                                                       <option value="431">CYPRUS</option>  '
    Div += '                                                       <option value="452">CZECH</option>  '
    Div += '                                                       <option value="274">DAGHYSTAN</option>  '
    Div += '                                                       <option value="315">DAHOOMI</option>  '
    Div += '                                                       <option value="361">DEM REP OF CONGO</option>  '
    Div += '                                                       <option value="423">DENMARK</option>  '
    Div += '                                                       <option value="141">DJIBOUTI</option>  '
    Div += '                                                       <option value="625">DOMINICAN</option>  '
    Div += '                                                       <option value="613">ECUADOR</option>  '
    Div += '                                                       <option value="125">EGYPT</option>  '
    Div += '                                                       <option value="635">EL SALVADOR</option>  '
    Div += '                                                       <option value="251">ENTIAGO</option>  '
    Div += '                                                       <option value="303">ERITREN</option>  '
    Div += '                                                       <option value="459">ESTONIA</option>  '
    Div += '                                                       <option value="317">ETHIOPIA</option>  '
    Div += '                                                       <option value="705">FIJI</option>  '
    Div += '                                                       <option value="433">FINLAND</option>  '
    Div += '                                                       <option value="403">FRANCE</option>  '
    Div += '                                                       <option value="659">FRENCH GUIANA</option>  '
    Div += '                                                       <option value="371">GABON</option>  '
    Div += '                                                       <option value="387">GAMBIA</option>  '
    Div += '                                                       <option value="273">GEORGIA</option>  '
    Div += '                                                       <option value="407">GERMANY</option>  '
    Div += '                                                       <option value="381">GHAMBIA</option>  '
    Div += '                                                       <option value="319">GHANA</option>  '
    Div += '                                                       <option value="385">GHINIA BISSAU</option>  '
    Div += '                                                       <option value="429">GREECE</option>  '
    Div += '                                                       <option value="365">GREENLAND</option>  '
    Div += '                                                       <option value="649">GRENADA</option>  '
    Div += '                                                       <option value="627">GUATAMALA</option>  '
    Div += '                                                       <option value="653">GUYANA</option>  '
    Div += '                                                       <option value="639">HAITI</option>  '
    Div += '                                                       <option value="409">HOLLAND</option>  '
    Div += '                                                       <option value="647">HONDURAS</option>  '
    Div += '                                                       <option value="223">HONG KONG</option>  '
    Div += '                                                       <option value="467">HUNGARY</option>  '
    Div += '                                                       <option value="443">ICELAND</option>  '
    Div += '                                                       <option value="205">INDIA</option>  '
    Div += '                                                       <option value="243">INDONESIA</option>  '
    Div += '                                                       <option value="201">IRAN</option>  '
    Div += '                                                       <option value="113">IRAQ</option>  '
    Div += '                                                       <option value="427">IRELAND</option>  '
    Div += '                                                       <option value="405">ITALY</option>  '
    Div += '                                                       <option value="323">IVORY COAST</option>  '
    Div += '                                                       <option value="629">JAMAICA</option>  '
    Div += '                                                       <option value="231">JAPAN</option>  '
    Div += '                                                       <option value="121">JORDAN</option>  '
    Div += '                                                       <option value="503">KAIMAN ISLAN</option>  '
    Div += '                                                       <option value="715">KALDUNIA NEW</option>  '
    Div += '                                                       <option value="261">KAZAKHESTAN</option>  '
    Div += '                                                       <option value="325">KENYA</option>  '
    Div += '                                                       <option value="667">KINGSTONE</option>  '
    Div += '                                                       <option value="391">KIRIBATI</option>  '
    Div += '                                                       <option value="476">KOSOVA</option>  '
    Div += '                                                       <option value="105">KUWAIT</option>  '
    Div += '                                                       <option value="489">KYRGYZ REPUBLIC</option>  '
    Div += '                                                       <option value="117">LABANON</option>  '
    Div += '                                                       <option value="245">LAOS</option>  '
    Div += '                                                       <option value="461">LATVIA</option>  '
    Div += '                                                       <option value="377">LESOTHO</option>  '
    Div += '                                                       <option value="327">LIBERIA</option>  '
    Div += '                                                       <option value="127">LIBYA</option>  '
    Div += '                                                       <option value="449">LIECHTENSTEIN</option>  '
    Div += '                                                       <option value="457">LITHUANIA</option>  '
    Div += '                                                       <option value="413">LUXEMBOURG</option>  '
    Div += '                                                       <option value="259">MACAU</option>  '
    Div += '                                                       <option value="329">MADAGASCAR</option>  '
    Div += '                                                       <option value="333">MALAWI</option>  '
    Div += '                                                       <option value="241">MALAYSIA</option>  '
    Div += '                                                       <option value="257">MALDIVES</option>  '
    Div += '                                                       <option value="335">MALI</option>  '
    Div += '                                                       <option value="435">MALTA</option>  '
    Div += '                                                       <option value="727">MARSHALL ISLAND</option>  '
    Div += '                                                       <option value="661">MARTINIQUE</option>  '
    Div += '                                                       <option value="721">MARYANA ISLAND</option>  '
    Div += '                                                       <option value="135">MAURITANIA</option>  '
    Div += '                                                       <option value="367">MAURITIUS</option>  '
    Div += '                                                       <option value="601">MEXICO</option>  '
    Div += '                                                       <option value="732">MICRONESIA</option>  '
    Div += '                                                       <option value="481">MOLDOVA</option>  '
    Div += '                                                       <option value="439">MONACO</option>  '
    Div += '                                                       <option value="249">MONGOLIA</option>  '
    Div += '                                                       <option value="488">MONTENEGRO</option>  '
    Div += '                                                       <option value="133">MOROCCO</option>  '
    Div += '                                                       <option value="337">MOZAMBIQUE</option>  '
    Div += '                                                       <option value="373">NAMEBIA</option>  '
    Div += '                                                       <option value="737">NAURU</option>  '
    Div += '                                                       <option value="235">NEPAL</option>  '
    Div += '                                                       <option value="707">NEW GHINIA</option>  '
    Div += '                                                       <option value="703">NEW ZEALAND</option>  '
    Div += '                                                       <option value="631">NICARAGUA</option>  '
    Div += '                                                       <option value="339">NIGER</option>  '
    Div += '                                                       <option value="341">NIGERIA</option>  '
    Div += '                                                       <option value="229">NORTH KOREA</option>  '
    Div += '                                                       <option value="421">NORWAY</option>  '
    Div += '                                                       <option value="723">OKINAWA</option>  '
    Div += '                                                       <option value="203">PAKISTAN</option>  '
    Div += '                                                       <option value="669">PALAU</option>  '
    Div += '                                                       <option value="123">PALESTINE</option>  '
    Div += '                                                       <option value="633">PANAMA</option>  '
    Div += '                                                       <option value="731">PAPUA NEW GUINE</option>  '
    Div += '                                                       <option value="645">PARAGUAY</option>  '
    Div += '                                                       <option value="615">PERU</option>  '
    Div += '                                                       <option value="237">PHILIPPINES</option>  '
    Div += '                                                       <option value="471">POLAND</option>  '
    Div += '                                                       <option value="425">PORTUGAL</option>  '
    Div += '                                                       <option value="641">PUERTO RICO</option>  '
    Div += '                                                       <option value="109">QATAR</option>  '
    Div += '                                                       <option value="485">REPUBL. OF MACEDONIA</option>  '
    Div += '                                                       <option value="490">REPUBLIC OF BELARUS</option>  '
    Div += '                                                       <option value="213">REPUBLIC OF MYANMAR</option>  '
    Div += '                                                       <option value="321">REPUPLIC OF GUINEA</option>  '
    Div += '                                                       <option value="469">ROMANIA</option>  '
    Div += '                                                       <option value="343">ROWANDA</option>  '
    Div += '                                                       <option value="477">RUSSIA</option>  '
    Div += '                                                       <option value="491">SAINT LUCIA</option>  '
    Div += '                                                       <option value="665">SAINT VINSENT</option>  '
    Div += '                                                       <option value="447">SAN MARINO</option>  '
    Div += '                                                       <option value="395">SAO TOME</option>  '
    Div += '                                                       <option value="103">SAUDI ARABIA</option>  '
    Div += '                                                       <option value="345">SENEGAL</option>  '
    Div += '                                                       <option value="383">SICHEL</option>  '
    Div += '                                                       <option value="347">SIERRA LEONE</option>  '
    Div += '                                                       <option value="225">SINGAPORE</option>  '
    Div += '                                                       <option value="454">SLOVAKIA</option>  '
    Div += '                                                       <option value="455">SLOVENIA</option>  '
    Div += '                                                       <option value="725">SOLOMON ISLAND</option>  '
    Div += '                                                       <option value="139">SOMALIA</option>  '
    Div += '                                                       <option value="349">SOUTH AFRICA</option>  '
    Div += '                                                       <option value="227">SOUTH KOREA</option>  '
    Div += '                                                       <option value="138">SOUTH SUDAN</option>  '
    Div += '                                                       <option value="437">SPAIN</option>  '
    Div += '                                                       <option value="217">SRI LANKA</option>  '
    Div += '                                                       <option value="606">ST HELENA</option>  '
    Div += '                                                       <option value="487">ST KITTS-NAVIS</option>    '
    Div += '                                                       <option value="137">SUDAN</option>  '
    Div += '                                                       <option value="111">SULTANATE OF OMAN</option>  '
    Div += '                                                       <option value="462">SURBIA</option>  '
    Div += '                                                       <option value="655">SURINAME</option>  '
    Div += '                                                       <option value="379">SWAZILAND</option>  '
    Div += '                                                       <option value="419">SWEDEN</option>  '
    Div += '                                                       <option value="415">SWIZERLAND</option>  '
    Div += '                                                       <option value="119">SYRIA</option>  '
    Div += '                                                       <option value="713">TAHITI</option>  '
    Div += '                                                       <option value="221">TAIWAN</option>  '
    Div += '                                                       <option value="267">TAJIKSTAN</option>  '
    Div += '                                                       <option value="353">TANZANIA</option>  '
    Div += '                                                       <option value="711">TASMANIA</option>  '
    Div += '                                                       <option value="239">THAILAND</option>  '
    Div += '                                                       <option value="483">THE HELLENIC REPBL</option>  '
    Div += '                                                       <option value="709">TIMOR LESTE</option>  '
    Div += '                                                       <option value="255">TONGA</option>  '
    Div += '                                                       <option value="637">TRINIDAD</option>  '
    Div += '                                                       <option value="129">TUNISIA</option>  '
    Div += '                                                       <option value="475">TURKEY</option>  '
    Div += '                                                       <option value="269">TURKMENISTAN</option>  '
    Div += '                                                       <option value="735">TUVALU</option>  '
    Div += '                                                       <option value="502">U S A</option>  '
    Div += '                                                       <option value="357">UGANDA</option>  '
    Div += '                                                       <option value="479">UKRAINE</option>  '
    Div += '                                                       <option value="643">URGWAY</option>  '
    Div += '                                                       <option value="271">UZBAKISTAN</option>  '
    Div += '                                                       <option value="505">United Nations</option>  '
    Div += '                                                       <option value="733">VANVATU</option>  '
    Div += '                                                       <option value="441">VATICAN</option>  '
    Div += '                                                       <option value="617">VENEZUELA</option>  '
    Div += '                                                       <option value="233">VIETNAM</option>  '
    Div += '                                                       <option value="729">W SAMOA</option>  '
    Div += '                                                       <option value="115">YEMEN</option>  '
    Div += '                                                       <option value="464">YUGOSLAVIA</option>  '
    Div += '                                                       <option value="359">ZAMBIA</option>  '
    Div += '                                                       <option value="331">ZIMBABWE</option>  '       //Country list//

    Div += '          </select>'
    Div += '        </div>'
    Div += '        </div>'
    Div += '    </div>'
    Div += '    </div>'
    ////////////*End First Row *///////

    ////////////* Second Row *///////
    Div += ' <div class="uk-form-row" >'
    Div += ' <div class="uk-grid" data-uk-grid-margin>'

    Div += '       <div class="uk-width-medium-1-3">'
    Div += '           <div class="uk-input-group">'
    Div += '             <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>'
    Div += '                    <label for="txtDepartureDate">Departure </label>'
    Div += '                     <input type="text" autocomplete="off" data-uk-datepicker="{format:"DD-MM-YYYY"}" data-trigger="focus" id="txtDepartureDate"  class="md-input validate[required] DepartureDate" />'
    Div += '           </div>'
    Div += '       </div>'

    Div += '       <div class="uk-width-medium-1-3">'
    Div += '           <div class="uk-input-group">'
    Div += '             <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>'
    Div += '                 <label for="txtArrivalDate">Arrival </label>'
    Div += '                    <input type="text" autocomplete="off" data-uk-datepicker="{format:"DD-MM-YYYY"}"  data-trigger="focus" id="txtArrivalDate"  class="md-input validate[required] ArrivalDate"/>'
    Div += '          </div>'
    Div += '       </div>'

    

    Div += '       <div class="uk-width-medium-1-3 divPan">'
    Div += '       <label> Pancard </label>'
    Div += '         <input type="text" autocomplete="off"  data-toggle="popover" data-placement="top" data-trigger="focus" id="txtPancard"  class="md-input validate[required] Pancard"  />'
    Div += '       </div>'

    Div += '     </div>'
    Div += '     </div>'
    ////////////* End Second Row *///////

   

    //Div += '<h3>Document Uploads</h3>'
    //Div += '<hr />'

    ////////////* Third Row *///////
    Div += ' <div class="uk-form-row">'
    Div += ' <div class="uk-grid" data-uk-grid-margin>'

    Div += '       <div class="uk-width-medium-1-3">'
    Div += '          <span class="uk-form-help-block">Photo</span>'
    Div += '            <div class="uk-form-file md-btn md-btn-primary">'
    Div += '              Choose File <input type="file" id="Photofile" class="validate[required] Photofile" accept="image/*" />'
    Div += '            </div>'
    Div += '        </div>'

    Div += '       <div class="uk-width-medium-1-3">'
    Div += '          <span class="uk-form-help-block">Passport First Page</span>'
    Div += '            <div class="uk-form-file md-btn md-btn-primary">'
    Div += '                Choose File  <input type="file" id="Passportfile" class="validate[required] Passportfile" accept="image/*" />'
    Div += '             </div>'
    Div += '        </div>'

    Div += '       <div class="uk-width-medium-1-3">'
    Div += '           <span class="uk-form-help-block">Last Page</span>'
    Div += '            <div class="uk-form-file md-btn md-btn-primary">'
    Div += '                Choose File <input type="file" id="Idfile" class="validate[required] Idfile" accept="image/*"  />'
    Div += '             </div>'
    Div += '       </div>'

    Div += '     </div>'
    Div += '     </div>'
    ////////////* End Third Row *///////

    Div += ' <div class="uk-form-row" style="margin-right:62%">'
    Div += ' <div class="uk-grid" data-uk-grid-margin>'
    Div += '</div>';
    Div += '</div>';

   
    ////////////* Fourth Row *///////
    Div += ' <div class="uk-form-row" >'
    Div += ' <div class="uk-grid" data-uk-grid-margin>'

    Div += '       <div class="uk-width-medium-1" >'
    // Div += '           <label></label></br>'
    Div += '         <button type="button" id="btn" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light add" onclick="AddMore()">Add Passenger</button> ';
    Div += '         <button type="button" id="btnDel" class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light remCF" > - </button>';
    Div += '      </div>';

    Div += '</div>';
    Div += '</div>';
    ////////////* End Fourth Row *///////

    Div += '</div>'
    Div += '</div>'
    Div += '</div>'


    //Div += '</div>';
    $("#Details").append(Div);
    var arrPax = $(".More");
    if (arrPax.length == 1) {
        $($('.More').find(".remCF")).hide();
    }
    else {
        $(arrPax).each(function (index, ndPax) {

            if (index != (arrPax.length - 1)) {
              
               $($(ndPax).find(".add")).hide();
            }

            if (index == 0)
                $($(ndPax).find(".remCF")).show();
        })
    }
    $(".remCF").on("click", function myfunction()
    {
        $(this).parent().parent().parent().parent().parent().parent().remove();
        var arrPax = $(".More");
        if (arrPax.length == 1)
        {
            $($('.More').find(".remCF")).hide();
            $($('.More').find(".add")).show();
        }
        else
            $(arrPax).each(function (index, ndPax)
            {
                if (index == (arrPax.length - 1))
                {
                    $($(ndPax).find(".remCF")).hide();

                    $($(ndPax).find(".add")).show();
                }
            })

        //$("#PaxCount").text(arrPax.length);
        //$("#PricePaxCount").text(arrPax.length);
        //$("#UrgentPricePaxCount").text(arrPax.length);
        $("#PaxCount").text(arrPax.length);
        $("#PricePaxCount").text(arrPax.length);
        $("#UrgentPricePaxCount").text(arrPax.length);
        var Price = getParameterByName('Price').replace("%20", " ");
        var Total = parseFloat(arrPax.length) * parseFloat(Price);
        var Urgent = $("#UrgentPrice").text();
        var SupTotal = parseFloat(Total) + parseFloat(parseFloat(Urgent) * parseFloat(arrPax.length));
        $("#TotalPrice").text(SupTotal);
    });

    // Datepicker('ED')
    //  Datepicker('ArrivalDate')
    // Datepicker('DepartureDate')

    $("form").validationEngine();
    $('.k-widget').removeClass('validate[required]');

    $("#PaxCount").text(arrPax.length);
    $("#PricePaxCount").text(arrPax.length);
    $("#UrgentPricePaxCount").text(arrPax.length);

    var Price = getParameterByName('Price').replace("%20", " ");
    var Total = parseFloat(arrPax.length) * parseFloat(Price);
    var Urgent = $("#UrgentPrice").text();
    var SupTotal = parseFloat(Total) + parseFloat(parseFloat(Urgent) * parseFloat(arrPax.length));
    $("#TotalPrice").text(SupTotal);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function MakeSummary() {
    $("#VisaDetail").empty();
    var Name = getParameterByName('Name').replace("%20", " ");
    var Price = getParameterByName('Price').replace("%20", " ");
    var Country = getParameterByName('Country').replace("%20", " ");

    var Details = "";

    Details += '     <div class="md-card-content">'
    Details += '         <div class="uk-grid" data-uk-grid-margin>'
    Details += '             <div class="uk-width-medium-1-4">'
    Details += '                 <div class="uk-form-row ">'
    Details += '                     <label>Visa Name : </label>'
    Details += '                     <label>' + Name.replace("%20", " ") + '</label>'
    Details += '                 </div>'
    Details += '             </div>'
    Details += '             <div class="uk-width-medium-1-4">'
    Details += '                 <div class="uk-form-row ">'
    Details += '                     <label>Country : </label>'
    Details += '                     <label>' + Country.replace("%20", " ") + '</label>'
    Details += '                 </div>'
    Details += '             </div>'
    Details += '             <div class="uk-width-medium-1-4">'
    Details += '                 <div class="uk-form-row ">'
    Details += '                     <label>Price :</label>'
    Details += '                     <label>' + Price.replace("%20", " ") + '</label>'
    Details += '                 </div>'
    Details += '             </div>'
    Details += '             <div class="uk-width-medium-1-4">'
    Details += '               <div class="uk-form-row ">'
    Details += '                     <input type="radio" name="VisaType"  onclick="GetvisaCharge(this.id)"  id="NormalCheck" /> <label> Normal </label>&nbsp;&nbsp;&nbsp; <input type="radio" name="VisaType"  onclick="GetvisaCharge(this.id)"  id="ExpressCheck" /> <label> Express </label>'
    Details += '                 </div>'
    Details += '             </div>'
    Details += '         </div>'
    Details += '     </div>'


    // Details += '<b >Visa Name </b> :  ' + Name.replace("%20", " ") + '      <b>Country  </b> :  ' + Country.replace("%20", " ") + '      <b>Price</b> :  ' + Price.replace("%20", " ");
    // Details += '&nbsp;&nbsp;&nbsp; <input type="radio" name="VisaType"  onclick="GetvisaCharge(this.id)"  id="NormalCheck" /> <label> Normal </label>&nbsp;&nbsp;&nbsp; <input type="radio" name="VisaType"  onclick="GetvisaCharge(this.id)"  id="ExpressCheck" /> <label> Express </label>'

    $("#VisaDetail").append(Details);

    $("#NormalCheck").attr('checked', true);

    $("#PaxCount").text(1);
    $("#PricePaxCount").text(1);
    $("#UrgentPricePaxCount").text(1);
    $("#UrgentPrice").text(0);

    $("#VisaPrice").text(Price);
    $("#TotalPrice").text(Price);

}

function GetvisaCharge(Id) {
    if (Id == "ExpressCheck") {
        var arrPax = $(".More");
        $("#PaxCount").text(arrPax.length);
        $("#PricePaxCount").text(arrPax.length);
        $("#UrgentPricePaxCount").text(arrPax.length);

        var Price = getParameterByName('Price').replace("%20", " ");
        $("#UrgentPrice").text(500);

        var Total = parseFloat(parseFloat(arrPax.length) * parseFloat(Price)) + parseFloat(parseFloat(500) * parseFloat(arrPax.length));
        $("#TotalPrice").text(Total);
    }
    else {
        var arrPax = $(".More");
        $("#PaxCount").text(arrPax.length);
        $("#PricePaxCount").text(arrPax.length);
        $("#UrgentPricePaxCount").text(arrPax.length);

        var Price = getParameterByName('Price').replace("%20", " ");
        $("#UrgentPrice").text(0);

        var Total = parseFloat(arrPax.length) * parseFloat(Price);
        $("#TotalPrice").text(Total);
    }

}

function Datepicker(type) {
    var ndDate = new Array();
    ndDate = $($('.' + type));
    $(ndDate).each(function (index, nd) {
        if (!$(nd).find('input').hasClass("k-input")) {
            $(nd).kendoDatePicker({   /*kendoCalendar*/
                format: "dd-MM-yyyy"
            });
        }
    });
}

function CheckNationality() {
    var arrPax = $(".More");
    $(arrPax).each(function (index, ndPax) {
        var Nation = $($(ndPax).find(".Nationality")).val();
        if (Nation == "205")
        {
            $($(ndPax).find(".Pancard")).prop('disabled', false);
            //$($(ndPax).find(".divPan")).show();
        }
        else {
            $($(ndPax).find(".Pancard")).prop('disabled', true);
           // $($(ndPax).find(".divPan")).hide();
        }
    });
}

function ChangeDeptDate() {
    var EDDate = $($(ndPax).find(".ArrivalDate").find('input:text')).val();
    var arrPax = $(".More");
    $(arrPax).each(function (index, ndPax) {


    });
}

function CheckEDDate() {
    var arrPax = $(".More");
    $(arrPax).each(function (index, ndPax) {
        var EDDate = $($(ndPax).find(".ED").find('input:text')).val();

        var today = new Date();
        today.setDate(today.getDate() + 182);

        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = dd + '-' + mm + '-' + yyyy;

        var ExDate = today;

        //if ( EDDate >  ExDate)
        //{
        //    alert("Expiry Date must 6 month remaining.");
        //    $($(ndPax).find(".ED").find('input:text')).val("");
        //}

    });
}


