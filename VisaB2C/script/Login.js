﻿
function Login()
{
    var email = $("#txt_email").val();
    if (email == "") {
        Success("Please enter Email");
        return false;
    }
    var password = $("#txt_password").val();
    if (password == "") {
        Success("Please enter password");
        return false;
    }
    var data = {
        email: email,
        password: password
    }
    $.ajax({
        type: "POST",
        url: "../handler/LoginDetail.asmx/UserLogin",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1)
            {
                var Name = $("#Name").val();
                var Price = $("#Price").val();
                var Country = $("#Country").val();
                //  alert("Send successfully.");
                window.location = "../User/AddVisaDetails.aspx?&Name=" + Name + "&Price=" + Price + "&Country=" + Country;
            }
            else {
                alert("Please try again.");
            }
        }
    });
}

function UserDetail() {
    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    //var regAddress = new RegExp('^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$');

    var Name = $("#txt_name").val();
    if (Name == "") {
        Success("Please enter User Name");
        return false;
    }
   
    var nMobile = $("#txt_mobileno").val();
    var title = $(".selected-flag");
    title = title[0].title;
    title = title.split(":");
    title = title[1].trim();
    if (nMobile.startsWith("+")) {
        nMobile = nMobile;
    }
    else {
        nMobile = title + " " + nMobile;
    }
    if (nMobile == "") {
        bValid = false;
        Success("Please Enter Mobile No");
        return;
    }
    else {
        if (!(reg.test(nMobile))) {
            bValid = false;
            Success("* Mobile no. must be numeric.");
            return;
        }
    }

    var email = $("#txt_mail").val();
    if (email == "") {
        Success("Please enter Email");
        return false;
    }
   
    var data = {
        Name: Name,
        nMobile: nMobile,
        email: email
    }
    $.ajax({
        type: "POST",
        url: "handler/LoginDetail.asmx/AddUserDetail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                alert("User details Register successfully.");
                window.location = "Default.aspx";
                
                
            }
            else if (obj.retCode == 2)
            {
                alert("Email ID or Mobile No is already Registered ,Please choose another .");
            }
            else
            {
                alert("Please try again.");
            }
        }
    });
}

function UpdateUser() {
    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    //var regAddress = new RegExp('^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$');
    var name = $("#Name").val();
    if (name == "") {
        Success("Please enter User Name");
        return false;
    }
   
    var useremail = $("#Email").val();
    if (useremail == "") {
        Success("Please enter Email");
        return false;
    }

    var useremail = $("#Email").val();
    if (useremail == "") {
        Success("Please enter Email");
        return false;
    }
    var Contact = $("#Contact").val();
    if (Contact == "") {
        bValid = false;
        Success("Please Enter Mobile No");
        return;
    }
    else {
        if (!(reg.test(Contact))) {
            bValid = false;
            Success("* Mobile no. must be numeric.");
            return;
        }
    }
    //var Password = $("#password").val();
    //if (Password == "") {
    //    Success("Please enter password");
    //    return false;
    //}
    //var repassword = $("#repassword").val();
    //if (repassword == "") {
    //    Success("Please enter reconfirm password");
    //    return false;
    //}
   
    //if (Password != repassword) {
    //    Success("Reconfirm password doesn't match");
    //    return false;
    //}
    var data = {
        name: name,
        useremail: useremail,
        Contact: Contact,
       
    }
    $.ajax({
        type: "POST",
        url: "../handler/LoginDetail.asmx/UpdateUser",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                alert(" User Profile Updated Successfully");
                window.location = "../User/EditProfile.aspx";
            }
            else {
                alert("Something Went Wrong");
                window.location.reload();
            }
        }
    });



}

function ForgetPass()
{
    var email = $("#txt_Useremail").val();
    if (email == "") {
        Success("Please enter Email");
        return false;
    }
    var data = {
        email: email
    }
    $.ajax({
        type: "POST",
        url: "handler/LoginDetail.asmx/ForgetPass",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                alert("Email Send successfully.");
                window.location = "Default.aspx";


            }
            else if (obj.retCode == 2) {
                alert("This Email is not Registered .");
            }
            else {
                alert("Please try again.");
            }
        }
    });

}







function Logout() {
    $.ajax({
        type: "POST",
        url: "../handler/LoginDetail.asmx/Logout",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                window.location.href = "../Default.aspx";
            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });



}


function checkUpdateSession() {
    $.ajax({
        url: "handler/LoginDetail.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode != 1 || result.retCode == 0) {
                $("#ModelLogin").modal('show');
            }
            else {
                $("#ModelProfileUpdate").modal('show');
            }
        },
        error: function () {
            alert('Error occured while authenticating user!');
            window.location.href = "Default.aspx";
        }
    });


}






