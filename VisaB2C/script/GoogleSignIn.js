﻿function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());

    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);
    $("#SignIn").hide();
    $("#LogOut").show();
    var Id = profile.getId();
    var name = profile.getName();
    var Email = profile.getEmail();
    var Img = profile.getImageUrl();
   
    // alert("Login Successfully.");
   // if (Name == null || Name == undefined && Price == null || Price== undefined) {
   //     $("#ModelLogin").modal('hide');
   // }
   if (Id != null || Id != undefined)
   {
       var Name = $("#Name").val();
       var Price = $("#Price").val();
       var Country = $("#Country").val();
           CheckSession();
        
      window.location = "../User/AddVisaDetails.aspx?&Name=" + Name + "&Price=" + Price + "&Country=" + Country;
    }
    
    var data = {
        Id: Id,
        name: name,
        Email: Email,
        profile: profile
    };

    $.ajax({
        type: "POST",
        url: "../handler/LoginDetail.asmx/CheckGoogleSession",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.response != 0) {
                var Name = $("#Name").val();
                var Price = $("#Price").val();
                var Country = $("#Country").val();
                CheckSession();
            }
           
        },
    });
};

window.fbAsyncInit = function () {
    // FB JavaScript SDK configuration and setup
    FB.init({
        appId: '2331184353819458', // FB App ID
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

    // Check whether the user already logged in
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            //display user data
            getFbUserData();
        }
    });
};

// Load the JavaScript SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, { scope: 'email' });
}

// Fetch the user profile data from facebook
function getFbUserData() {
    FB.api('/me', { locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
    function (response) {
        document.getElementById('fbLink').setAttribute("onclick", "fbLogout()");
        document.getElementById('fbLink').innerHTML = 'Logout from Facebook';
        document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.first_name + '!';
        document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> ' + response.id + '</p><p><b>Name:</b> ' + response.first_name + ' ' + response.last_name + '</p><p><b>Email:</b> ' + response.email + '</p><p><b>Gender:</b> ' + response.gender + '</p><p><b>Locale:</b> ' + response.locale + '</p><p><b>Picture:</b> <img src="' + response.picture.data.url + '"/></p><p><b>FB Profile:</b> <a target="_blank" href="' + response.link + '">click to view profile</a></p>';

        if (response.id != null || response.id != undefined)
        {
            var Name = $("#Name").val();
            var Price = $("#Price").val();
            var Country = $("#Country").val();
            window.location = "../User/AddVisaDetails.aspx?&Name=" + Name + "&Price=" + Price + "&Country=" + Country;
        }


    });
}

// Logout from facebook
function fbLogout() {
    FB.logout(function () {
        document.getElementById('fbLink').setAttribute("onclick", "fbLogin()");
        document.getElementById('fbLink').innerHTML = '<img src="images/fb-login.png" style="width: 90%; margin-top:-6px; cursor:pointer" />';
        document.getElementById('userData').innerHTML = '';
        document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
    });
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
        alert('User signed out.');
        $("#SignIn").show();
        $("#LogOut").hide();
    });
}



