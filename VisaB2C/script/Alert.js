﻿function Success(msg) {
    $.modal({
        contentBg: false,
        contentAlign: 'center',
        content: '<span class="white"><b>' + msg + '<b><span>',
        resizable: false,
        spacing: 5,
        classes: ['black-gradient'],
        animateMove: 1,
        buttons: {
            'Close': {
                classes: 'green-gradient small',
                click: function (modal) { modal.closeModal(); }
            }

        },
        buttonsAlign: 'right'
    });
}

function Confirm(msg, Method, id) {
    $.modal.confirm(msg, function () {
        var id = [];
        if (arg != null) {
            for (var i = 0; i < arg.length; i++) {
                id.push('"' + arg[i] + '"')
            }
        }
        if (arg == null)
            $(".blue-gradient").setAttribute("onclick", Method + "()")
        else
            $(".blue-gradient").setAttribute("onclick", Method + "(" + id + ")")
    }, function () {
        $('#modals').remove();
    });
}

function AlertDanger(msg) {
    notify(msg, {
        icon: '../img/demo/error.png',
        system: true
    });
}