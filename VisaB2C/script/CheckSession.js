﻿
function CheckSession() {
    $.ajax({
        url: "handler/LoginDetail.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode != 1 || result.retCode == 0)
            {
                $("#ModelLogin").modal('show');
            }
            else {
                var Name = $("#Name").val();
                var Price = $("#Price").val();
                var Country = $("#Country").val();
                window.location = "../User/AddVisaDetails.aspx?&Name=" + Name + "&Price=" + Price + "&Country=" + Country;
            }
        },
        error: function () {
            alert('Error occured while authenticating user!');
            window.location.href = "../Default.aspx";
        }
    });
}


function CheckSessionDefault() {
    debugger;
    $.ajax({
        url: "../handler/LoginDetail.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode != 1 || result.retCode == 0)
            {
                alert('Your session expire.');
                window.location = "../Default.aspx";
            }
            else
            {
            }
        },
        error: function () {
            alert('Your session expire.');
            window.location.href = "../Default.aspx";
        }
    });
}

