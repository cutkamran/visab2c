﻿using B2CVacaay.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using VisaB2C.dbml;

namespace VisaB2C.Datalayer
{
    public class LoginUser
    {
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        public static string AddUser(string Name, string nMobile, string email)
        {
            string json = "";

            using (var DB = new helperDataContext())
            {
                try
                {
                    var UserData = (from user in DB.tbl_B2C_CustomerLogins where user.Email == email || user.Contact1 == nMobile select user).FirstOrDefault();
                    if (UserData == null)
                    {
                        var Password = Crypto.GenerateRandomString(8);
                        var EncryptedPassword = Crypto.EncryptText(Password);
                        tbl_B2C_CustomerLogin Add = new tbl_B2C_CustomerLogin();
                        Add.Name = Name;
                        Add.UserName = email;
                        Add.Contact1 = nMobile;
                        Add.Email = email;
                        Add.Password = EncryptedPassword;
                        DB.tbl_B2C_CustomerLogins.InsertOnSubmit(Add);
                        DB.SubmitChanges();
                        emailmanager.SendLoginMail(Name, email, Password);
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                    else
                    {
                        json = "{\"Session\":\"1\",\"retCode\":\"2\"}";
                    }

                }
                catch (Exception ex)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

            }
            return json;

        }

        public static string UpdateUserDetail(string name, string useremail, string Contact)
        {
            string json = "";
            UserInfo objUserInfo = (UserInfo)HttpContext.Current.Session["LoginUser"];
            using (var DB = new helperDataContext())
            {
                try
                {
                    tbl_B2C_CustomerLogin update = DB.tbl_B2C_CustomerLogins.Single(x => x.Sid == objUserInfo.Sid);
                    update.Name = name;
                    update.Email = useremail;
                    update.Contact1 = Contact;
                     objUserInfo = (UserInfo)HttpContext.Current.Session["LoginUser"];
                    objUserInfo.Name = name;
                    objUserInfo.Email = useremail;
                    objUserInfo.Contact1 = Contact;
                    //update.Password = Crypto.EncryptText(Password);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";


                }
                catch (Exception ex)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

            }
            return json;


        }



        public static string UserLoginDetail(string email, string password)
        {
            UserInfo objUserInfo;
            using (var DB = new helperDataContext())
            {
                string json = "";
                try
                {
                    var Pass = Crypto.EncryptText(password);
                    var List = (from user in DB.tbl_B2C_CustomerLogins where user.UserName == email && user.Password == Pass select user).FirstOrDefault();

                    if (List != null)
                    {
                        objUserInfo = new UserInfo();
                        objUserInfo.Sid = List.Sid;
                        objUserInfo.Name = List.Name;
                        objUserInfo.Contact1 = List.Contact1;
                        objUserInfo.Email = List.Email;
                        objUserInfo.Password = Crypto.DecryptText(List.Password);
                        HttpContext.Current.Session["LoginUser"] = objUserInfo;
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }

                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                catch (Exception ex)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return json;
            }

        }

        public static string ForgetPass(string email)
        {
            using (var DB = new helperDataContext())
            {
                string json = "";
                try
                {
                    var List = (from user in DB.tbl_B2C_CustomerLogins where user.UserName == email select user).FirstOrDefault();
                    if (List != null)
                    {
                        var DecryptedPassword = Crypto.DecryptText(List.Password);
                        emailmanager.SendLoginMail(List.Name, email, DecryptedPassword);
                        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    }
                    else
                        json = "{\"Session\":\"1\",\"retCode\":\"2\"}";

                }
                catch (Exception ex)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return json;
            }
        }



       
        public static string GoogleSession(string Id, string name, string Email)
        {
            var json = "";
            using (var db = new dbml.helperDataContext())
            {
                var UserMail = (from x in db.tbl_B2C_CustomerLogins where x.Email == Email select x).FirstOrDefault();
                string UniqueCode = "";
            
                if (UserMail == null)
                {
                    var Password = Crypto.GenerateRandomString(8);
                    var EncryptedPassword = Crypto.EncryptText(Password);
                    UniqueCode = "CUK-" + Crypto.GenerateRandomString(8);
                    tbl_B2C_CustomerLogin User = new tbl_B2C_CustomerLogin();
                    User.UserName = Email;
                    User.Email = Email;
                    User.B2C_Id = UniqueCode;
                    User.Password = EncryptedPassword;
                    db.tbl_B2C_CustomerLogins.InsertOnSubmit(User);
                    db.SubmitChanges();
                }
                UserInfo objUserInfo = new UserInfo();
                objUserInfo.Email = Email;
                objUserInfo.Name = name;
                HttpContext.Current.Session["LoginUser"] = objUserInfo;

            }
            json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            return json;
        }
        public static string CheckUserSession()
        {
            var data = HttpContext.Current.Session["LoginUser"];
            using (var DB = new helperDataContext())
            {
                string json = "";
                try
                {
                    if (HttpContext.Current.Session["LoginUser"] != null)
                        return "{\"Session\":\"1\",\"retCode\":\"1\"}";
                    else
                        return "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                catch (Exception)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
                return json;
            }
        }

        public static string GetUploadedDocs()
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            using (var DB = new helperDataContext())
            {
                string json = "";
                try
                {
                    UserInfo objUserInfo = (UserInfo)HttpContext.Current.Session["LoginUser"];
                    var List = (from meta in DB.tbl_VisaDetails where meta.UserId == objUserInfo.Sid select meta.Vcode).ToList();

                    json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = List });
                }
                catch (Exception ex)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }

                return json;
            }


        }

        public static string GetUserProfile()
        {
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            UserInfo objUserInfo = (UserInfo)HttpContext.Current.Session["LoginUser"];
            string json = "";
            try
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = objUserInfo });
            }
            catch (Exception)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;


        }
    }

}