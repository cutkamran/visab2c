﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace VisaB2C.Datalayer
{
    public class emailmanager
    {
        public static bool SendMail(string userName, string nMobile, string email, string subject, string message)
        {
            //Work start
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            StringBuilder AdminMail = new StringBuilder();
            AdminMail.Append("<div style=\"font-family:Segoe UI,Tahoma,sans-serif;margin:0px 40px 0px 40px;width:auto;\">");
            AdminMail.Append("<div style=\"min-height:50px;width:auto\">");
            AdminMail.Append("<br>");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div style=\"margin-left:10%\">  ");
            AdminMail.Append("<span style=\"margin-left:2%;font-weight:400\">Hello " + userName + ",</span><br />");
            AdminMail.Append("<p> <span>Your Information Details.</span><br></p>  ");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div><table>");
            AdminMail.Append("<tbody><tr>");
            AdminMail.Append("<td><b>Name</b></td>");
            AdminMail.Append("<td>: " + userName + "</td>");
            AdminMail.Append("");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>E-Mail</b></td>");
            AdminMail.Append("<td>:  " + email + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Mobile No.</b></td>");
            AdminMail.Append("<td>:  " + nMobile + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Subject : </b></td>");
            AdminMail.Append("<td>:  " + subject + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Message : </b></td>");
            AdminMail.Append("<td>:  " + message + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("</tbody></table></div>");
            AdminMail.Append("<span>");
            AdminMail.Append("<b>Thank You,</b><br>");
            AdminMail.Append("</span>");
            AdminMail.Append("<span>");
            AdminMail.Append("Administrator");
            AdminMail.Append("</span>");
            AdminMail.Append("");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div>");
            AdminMail.Append("<span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><table border=\"0\" style=\"height:30px;width:100%;border-spacing:0px;border-top-color:white;border-left:none;border-right:none\">");
            AdminMail.Append("<tbody><tr style=\"border:none;border-bottom-color:gray;color:#57585a\">");
            AdminMail.Append("</font></span></div><span class=\"HOEnZb adL\"><font color=\"#888888\">");
          
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                var to = Convert.ToString(ConfigurationManager.AppSettings["ContactMail"]);
                Email1List.Add(to, to);
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                bool reponse = SendMail(accessKey, Email1List, "User Contact Details", AdminMail.ToString(), from, DocLinksList);

                return reponse;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendAssesmentMail(string assesmentName, string assesemail, string mobileno, string visatype, string assesmessage)
        {
            //Work start
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            StringBuilder AdminMail = new StringBuilder();
            AdminMail.Append("<div style=\"font-family:Segoe UI,Tahoma,sans-serif;margin:0px 40px 0px 40px;width:auto;\">");
            AdminMail.Append("<div style=\"min-height:50px;width:auto\">");
            AdminMail.Append("<br>");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div style=\"margin-left:10%\">  ");
            AdminMail.Append("<span style=\"margin-left:2%;font-weight:400\">Hello " + assesmentName + ",</span><br />");
            AdminMail.Append("<p> <span>Book Free Assesment Information Details.</span><br></p>  ");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div><table>");
            AdminMail.Append("<tbody><tr>");
            AdminMail.Append("<td><b>User Name</b></td>");
            AdminMail.Append("<td>: " + assesmentName + "</td>");
            AdminMail.Append("");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>E-Mail</b></td>");
            AdminMail.Append("<td>:  " + assesemail + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Mobile No.</b></td>");
            AdminMail.Append("<td>:  " + mobileno + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Visa Type : </b></td>");
            AdminMail.Append("<td>:  " + visatype + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Assesment Message : </b></td>");
            AdminMail.Append("<td>:  " + assesmessage + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("</tbody></table></div>");
            AdminMail.Append("<span>");
            AdminMail.Append("<b>Thank You,</b><br>");
            AdminMail.Append("</span>");
            AdminMail.Append("<span>");
            AdminMail.Append("Administrator");
            AdminMail.Append("</span>");
            AdminMail.Append("");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div>");
            AdminMail.Append("<span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><table border=\"0\" style=\"height:30px;width:100%;border-spacing:0px;border-top-color:white;border-left:none;border-right:none\">");
            AdminMail.Append("<tbody><tr style=\"border:none;border-bottom-color:gray;color:#57585a\">");
            AdminMail.Append("</font></span></div><span class=\"HOEnZb adL\"><font color=\"#888888\">");
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));
                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();
                var to = Convert.ToString(ConfigurationManager.AppSettings["ContactMail"]);
                Email1List.Add(to, to);
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                bool reponse = SendMail(accessKey, Email1List, "Book Free assesment Detail", AdminMail.ToString(), from, DocLinksList);

                return reponse;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendLoginMail(string Name, string email, string Password)
        {
            //Work start
            JavaScriptSerializer objSerializer = new JavaScriptSerializer();
            StringBuilder AdminMail = new StringBuilder();
            AdminMail.Append("<div style=\"font-family:Segoe UI,Tahoma,sans-serif;margin:0px 40px 0px 40px;width:auto;\">");
            AdminMail.Append("<div style=\"min-height:50px;width:auto\">");
            AdminMail.Append("<br>");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div style=\"margin-left:10%\">  ");
            AdminMail.Append("<span style=\"margin-left:2%;font-weight:400\">Hello " + Name + ",</span><br />");
            AdminMail.Append("<p> <span>Your Information Details.</span><br></p>  ");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div><table>");
            AdminMail.Append("<tbody>");
            //AdminMail.Append("<tr>");
            //AdminMail.Append("<td><b>User Name</b></td>");
            //AdminMail.Append("<td>: " + Name + "</td>");
            //AdminMail.Append("");
            //AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>User Name</b></td>");
            AdminMail.Append("<td>:  " + email + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("<tr>");
            AdminMail.Append("<td><b>Password  </b></td>");
            AdminMail.Append("<td>:  " + Password + "</td>");
            AdminMail.Append("</tr>");
            AdminMail.Append("</tbody></table></div>");
            AdminMail.Append("<span>");
            AdminMail.Append("<b>Thank You,</b><br>");
            AdminMail.Append("</span>");
            AdminMail.Append("<span>");
            AdminMail.Append("Administrator");
            AdminMail.Append("</span>");
            AdminMail.Append("");
            AdminMail.Append("</div>");
            AdminMail.Append("");
            AdminMail.Append("");
            AdminMail.Append("<div>");
            AdminMail.Append("<span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><span class=\"HOEnZb\"><font color=\"#888888\">");
            AdminMail.Append("</font></span><table border=\"0\" style=\"height:30px;width:100%;border-spacing:0px;border-top-color:white;border-left:none;border-right:none\">");
            AdminMail.Append("<tbody><tr style=\"border:none;border-bottom-color:gray;color:#57585a\">");
            AdminMail.Append("</font></span></div><span class=\"HOEnZb adL\"><font color=\"#888888\">");
            try
            {
                List<string> from = new List<string>();
                from.Add(Convert.ToString(ConfigurationManager.AppSettings["supportMail"]));

                List<string> DocLinksList = new List<string>();
                Dictionary<string, string> Email1List = new Dictionary<string, string>();

                Email1List.Add(email, email);
                string accessKey = Convert.ToString(ConfigurationManager.AppSettings["AccessKey"]);
                bool reponse = SendMail(accessKey, Email1List, "Your Password Detail", AdminMail.ToString(), from, DocLinksList);

                return reponse;
            }
            catch
            {
                return false;
            }
        }
        public static bool SendMail(string Key,
                           Dictionary<string, string> to,
                           string subject,
                           string MailBody,
                           List<string> from_name,
                           List<string> attachment)
        {
            try
            {
                string accessKey = ConfigurationManager.AppSettings["AccessKey"];

                // API test = new API(accessKey);
                API test = new API(accessKey);

                Dictionary<string, Object> data = new Dictionary<string, Object>();

                data.Add("to", to);
                //data.Add("bcc", to);
                data.Add("from", from_name);
                data.Add("subject", subject);
                data.Add("html", MailBody);
                if (attachment != null && attachment.Count != 0)
                    data.Add("attachment", attachment);
                Object sendEmail = test.send_email(data);
                //return sendEmail.ToString();
                return true;
            }
            catch
            {
                return false;
            }

        }

    }



}

