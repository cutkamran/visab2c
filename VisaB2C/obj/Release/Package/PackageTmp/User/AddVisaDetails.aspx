﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.Master" AutoEventWireup="true" CodeBehind="AddVisaDetails.aspx.cs" Inherits="VisaB2C.User.VisaApply" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/jquery-1.10.2.min.js"></script>
    <script src="../script/CheckSession.js"></script>
    <link rel="stylesheet" href="../js/formValidator/developr.validationEngine.css?v=1" />
    <script src="../script/AddVisaDetails.js?v=1.1"></script>

    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>

    <script>
        //$(window).unload(function () {
        //    unloadPage()
        //});

        $(function () {
            $("form").validationEngine();

            var oldValue;
            var unsaved = false;
            //$(":input").change(function () {
            //    unsaved = true;
            //});
            $('input[type="text"]').keyup(function () {
                debugger;
                // = this.value()
                if (/^[A-Z0 -9]+$/i.test(this.value)) {
                }
                else {
                    this.value = '';
                }
            });
        });

        //jQuery(window).bind('beforeunload', function () {
        //    return 'You sure you want to leave?';
        //});

        function GetPrepareRazorPayModal() {
            CheckSessionDefault();
            document.getElementById("rzp-button1").click();
            var total = $("#TotalPrice").text();
            var TotalFare = parseFloat(total.replace(",", ""));
            var options = {
                // "key": "rzp_live_Q8PFvYZQP356qb",
                "key": "rzp_live_0tFTmrMGQkOcZa",

                "order_id": "001",

                "amount": TotalFare * 100, // 2000 paise = INR 20
                "name": "VIZACafe",
                "description": "Visa Booking",
                "currency": "INR",
                "image": "https://trivo.in/images/logo.png",
                "handler": function (response) {
                    alert(response.razorpay_payment_id);
                    CheckSessionOnPage();
                    window.alert('Thank you For Booking.');
                },
                "prefill": {
                    "name": "A Q",
                    "email": "sheikh.quddus@gmail.com",
                },
                "notes": {
                    "address": ""
                },
                "theme": {
                    "color": "#f58634"
                }
            };
            var rzp1 = new Razorpay(options);

            document.getElementById('rzp-button1').onclick = function (e) {
                rzp1.open();
                e.preventDefault();
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="page_content">
        <div id="page_content_inner">

                   <div class="md-card" id="VisaDetail"> </div>
                
                <%--<div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-4">
                                <div class="uk-form-row ">
                                    <label>Visa Name :</label>
                                    <label>Visa Name :</label>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-4">
                                <div class="uk-form-row ">
                                    <label>Visa Name :</label>
                                    <label>Visa Name :</label>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-4">
                                <div class="uk-form-row ">
                                    <label>Visa Name :</label>
                                    <label>Visa Name :</label>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-4">
                                <div class="uk-form-row ">
                                    <label>Visa Name :</label>
                                    <label>Visa Name :</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>

            <div class="uk-grid" data-uk-grid-margin>

                <div id="Details" class="uk-width-medium-3-4"></div>


                <%--<div class="uk-width-medium-3-4">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-grid" data-uk-grid-margin>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <div class="uk-width-medium-1-4">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-width-medium-5-5">
                                <div class="sidebar">
                                    <div class="widget widget-quote-form bg-yellow">
                                        <h3 class="form-title">Summary</h3>

                                        <!-- Text input-->
                                        <div class="form-group">
                                            <div class="">
                                                <label class="">Passanger Count : </label>
                                                <label class="" id="PaxCount"></label>
                                                <br />
                                                <label class="">Visa Price : </label>
                                                <label class="" id="VisaPrice"></label>
                                                * 
                                               <label class="" id="PricePaxCount"></label>
                                                <br />
                                                <label class="" id="lblUrgentPrice">Urgent Price : </label>
                                                <label class="" id="UrgentPrice"></label>
                                                *   
                                                <label class="" id="UrgentPricePaxCount"></label>

                                                <br />
                                                <hr />

                                                <label class="">Total Price : </label>
                                                <label class="" id="TotalPrice"></label>
                                                  
                                            </div>
                                        </div>
                                               <br />
                                        <button type="button" id="RazorButton" onclick="CheckSessionOnPage()" class="md-btn md-btn-danger md-btn-wave-light waves-effect waves-button waves-light">Pay With RazorPay</button>
                                        <%-- <button id="RazorButton"  onclick="GetPrepareRazorPayModal()"  class="btn btn-default">Pay With RazorPay</button>
                                 <button  class="" id="rzp-button1" style="float:left;display:none">Pay</button>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
