﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.Master" AutoEventWireup="true" CodeBehind="VisaList.aspx.cs" Inherits="VisaB2C.User.VisaList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
     <script src="../js/jquery-2.1.3.min.js"></script>
   <%-- <script src="../script/UserDetails.js"></script>--%>
    <script src="../script/Visa.js"></script>
    <%--<script src="../js/DataTables/jquery.dataTables.min.js"></script>--%>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page_content">
        <div id="page_content_inner">

            <h3 class="heading_b uk-margin-bottom">Visa List</h3>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table  id="tbl_VisaDetails"  class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th >Sr.No:</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Visa Type</th>
                            <th>Visa Code</th>
                            <th>Passport No</th>
                            <th>Expiry Date</th>
                            <th>Arrival Date</th>
                            <th>Departure Date</th>
                            <th>Visa Fees</th>
                            <th>Total Amount</th>
                        </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
   
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function () {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
</asp:Content>
