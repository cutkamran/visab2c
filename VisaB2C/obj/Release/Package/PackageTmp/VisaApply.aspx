﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="VisaApply.aspx.cs" Inherits="VisaB2C.VisaApply" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contact us</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="table-responsive">
                    <table width="100%" cellspacing="0" cellpadding="7" border="0" class="table no-margin Visa-table table-responsive">
                        <tbody id="add_Visa_travellers">
                            <input type="hidden" name="adult" id="Visa_adult" value="" onkeyup="removeErrorMessage()">
                            <input type="hidden" name="child" id="Visa_child" value="" onkeyup="removeErrorMessage()">
                            <input type="hidden" name="name" id="Visa_name" value="" onkeyup="removeErrorMessage()">
                            <input type="hidden" id="count_traveller" value="2">

                            <tr class="Visa_travellers" id="Visa_travellers_1">
                                <td align="left" valign="top" width="30%">Traveller 1 :</td>
                                <td align="right" valign="top" width="20%" class="text-right">
                                    <input type="text" name="Visa_firstname[]" id="Visa_firstname1" class="GetProposal Visa_firstname1" value="" onkeyup="removeErrorMessage()" placeholder="First Name">
                                </td>
                                <td align="right" valign="top" width="20%" class="text-right">
                                    <input type="text" name="Visa_lastname[]" id="Visa_lastname1" class="GetProposal Visa_lastname1" value="" onkeyup="removeErrorMessage()" placeholder="Last Name">
                                </td>
                                <td align="right" valign="top" width="20%" class="text-right"><span class="text-right ">
                                    <select id="pax_type" name="Visa_pax_type[]" placeholder="Select" class="GetProposal1">
                                        <option value="1">Adult</option>
                                        <option value="2">Child</option>
                                        <option value="3">Infant</option>
                                    </select>

                                </span>
                                </td>
                                <td align="right" valign="top" class="text-right" width="10%"><a href="javascript://" class="btn btn-success btn-sm" onclick="addVisaMoreTravellers()"><i class="fa fa-plus"></i></a></td>

                            </tr>
                        </tbody>
                    </table>

                    <table width="100%" cellspacing="0" cellpadding="7" border="0" class="table no-margin Visa-table table-responsive">
                        <tbody>
                            <tr>
                                <td valign="top" align="left" class="text-left" width="30%">Email :</td>
                                <td valign="top" align="right" class="text-right" width="20%">
                                    <input type="text" name="email" id="Visa_email" class="GetProposal" value="" onkeyup="removeErrorMessage()"></td>
                                <td valign="middle" align="right" class="text-right " width="20%"><span class="text-left">Travel Date :</span></td>
                                <td valign="middle" align="right" class="text-right" width="20%">
                                    <input type="text" name="date" class="GetProposal datepicker hasDatepicker" id="Visa_date" value="" onkeyup="removeErrorMessage()" maxlength="10"></td>
                                <td valign="middle" align="right" class="text-right " width="10%">
                                    <div style="width: 120px;">&nbsp;</div>
                                </td>
                            </tr>

                            <tr>
                                <td valign="middle" align="left" class="">Mobile :</td>
                                <td valign="middle" align="right" class="text-right "><span class="text-right ">
                                    <input type="text" name="mobile" id="Visa_mobile" class="GetProposal" value="" onkeyup="removeErrorMessage()" maxlength="10">
                                </span></td>
                                <td valign="middle" align="right" class="text-right">&nbsp;</td>
                                <td valign="middle" align="right" class="text-right">&nbsp;</td>
                                <td valign="middle" align="right" class="text-right">&nbsp;</td>
                            </tr>


                            <tr>
                                <td colspan="5" align="left" valign="middle" class="price price1 text-right"><a href="JavaScript:void(0);" class="btn btn-default" id="getbtnBack" style="background-color: #e6e6e6;">Back</a> <a href="JavaScript:void(0);" class="btn btn-success" id="getbtn">Get Proposal on email</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
