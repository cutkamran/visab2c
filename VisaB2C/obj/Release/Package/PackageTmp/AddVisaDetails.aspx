﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AddVisaDetails.aspx.cs" Inherits="VisaB2C.AddVisaDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- <script src="js/jquery-2.1.3.min.js"></script>--%>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="script/CheckSession.js"></script>
   <link rel="stylesheet" href="js/formValidator/developr.validationEngine.css?v=1" />   
   <script src="script/AddVisaDetails.js?v=1.1"></script>

    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
   
    <script>
        //$(window).unload(function () {
        //    unloadPage()
        //});

        $(function ()
        {
            $("form").validationEngine();

            var oldValue;
            var unsaved = false;
            //$(":input").change(function () {
            //    unsaved = true;
            //});
            $('input[type="text"]').keyup(function () {
                debugger;
                // = this.value()
                if (/^[A-Z0 -9]+$/i.test(this.value)) {
                }
                else {
                    this.value = '';
                }
            });
        });

        //jQuery(window).bind('beforeunload', function () {
        //    return 'You sure you want to leave?';
        //});

        function GetPrepareRazorPayModal() {
            CheckSessionDefault();
            document.getElementById("rzp-button1").click();
            var total = $("#TotalPrice").text();
            var TotalFare = parseFloat(total.replace(",", ""));
            var options = {
                // "key": "rzp_live_Q8PFvYZQP356qb",
                "key": "rzp_live_0tFTmrMGQkOcZa",

                "order_id": "001",

                "amount": TotalFare * 100, // 2000 paise = INR 20
                "name": "VIZACafe",
                "description": "Visa Booking",
                "currency": "INR",
                "image": "https://trivo.in/images/logo.png",
                "handler": function (response) {
                    alert(response.razorpay_payment_id);
                    CheckSessionOnPage();
                    window.alert('Thank you For Booking.');
                },
                "prefill": {
                    "name": "A Q",
                    "email": "sheikh.quddus@gmail.com",
                },
                "notes": {
                    "address": ""
                },
                "theme": {
                    "color": "#f58634"
                }
            };
            var rzp1 = new Razorpay(options);

            document.getElementById('rzp-button1').onclick = function (e) {
                rzp1.open();
                e.preventDefault();
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />

    <!-- CONTENT -->
    <div class="content">
        <div class=" container">
            <%-- <div class="section-title text-center">
                <h1 class="mb0">Visa Application Form</h1>
                <hr />
            </div>
            <br />--%>

            <div class="row">
                <div class="col-xl-12 col-lg-5 col-md-7 col-sm-12 col-12 bg-yellow mb-2 ">
                    <div id="VisaDetail">
                    </div>
                </div>
            </div>

            <div class="row">
                    <div class="col-xl-9 col-lg-5 col-md-7 col-sm-12 col-12" id="Details">

                        <%-- <h3>Passenger Details</h3>
                    <hr />
                    <div class="row">
                        <div class="col-md-4">
                            First Name<label style="color: red; margin-top: 3px;">
                                <b>*</b></label>
                            :                                       
                            <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtFirst" placeholder="First Name" class="form-control" onchange="CheckValidation(this.id)" />
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtFirst">
                                <b>* This field is required</b></label>
                        </div>
                        <div class="col-md-4">
                            Middle Name :
                           
                                <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtMiddle" placeholder=" Middle Name" class="form-control" />
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtMiddle">
                                <b>* This field is required</b></label>
                        </div>
                        <div class="col-md-4">
                            Last Name<label style="color: red; margin-top: 3px;">
                                <b>*</b></label>
                            :
                             <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtLast" placeholder=" Last Name" class="form-control" onchange="CheckValidation(this.id)" />
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtLast">
                                <b>* This field is required</b></label>
                        </div>
                    </div>

                    <br />
                    <h3>Passport Details</h3>
                    <hr />
                    <div class="row">
                        <div class="col-md-4">
                            <label>Passport No</label>
                            <label style="color: red; margin-top: 3px;">
                                <b>*</b></label>
                            :
                           
                                <input type="text" data-toggle="popover" data-placement="top" data-trigger="focus" id="txtPassport" placeholder="Passport Number" class="form-control" onchange="CheckValidation(this.id)" />
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtPassport">
                                <b>* This field is required</b></label>
                        </div>

                        <div class="col-md-4">
                            <label>Expiry Date</label>
                            <label style="color: red; margin-top: 3px;">
                                <b>*</b></label>
                            :
                            
                                <input type="text" id="txtED" placeholder="dd-mm-yyyy" class="form-control mySelectCalendar" style="cursor: pointer" onchange="CheckValidation(this.id)" />
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_txtED">
                                <b>* This field is required</b></label>
                        </div>

                        <div class="col-md-4">
                            <label>Nationality</label>
                            <label style="color: red; margin-top: 3px;">
                                <b>*</b></label>:
                                   
                                        <select id="selVisaCountry" class="form-control" onchange="Notification()">
                                            <option value="-" selected="selected">--Salect Visa Country--</option>
                                            <option value="Indian">Indian</option>
                                            <option value="Pakistani">Pakistani</option>
                                            <option value="Sri Lankan">Sri Lankan</option>
                                            <option value="South African">South African</option>
                                            <option value="Mozambican">Mozambican</option>
                                            <option value="Malaysian">Malaysian</option>
                                            <option value="Indonesian">Indonesian</option>
                                            <option value="Philippines">Philippines</option>
                                        </select>
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_selVisaCountry">
                                <b>* This field is required</b></label>
                        </div>

                    </div>

                    <br />
                    <h3>Document Uploads</h3>
                    <hr />
                    <div class="row">

                        <div class="col-md-4">
                            <label>Passport</label>
                            <input type="file" data-trigger="focus" id="passportfile" class="form-control" />
                        </div>

                        <div class="col-md-4">
                            <label>Photo</label>
                            <input type="file" data-trigger="focus" id="Photofile" class="form-control" />
                        </div>

                        <div class="col-md-4">
                            <label>Id</label>
                            <input type="file" data-trigger="focus" id="Idfile" class="form-control" />
                        </div>

                    </div>

                    <div class="row">
                    <div class="col-md-4">
                    </div>
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <br />
                            <button type="button" id="btn1" class="btn btn-default" onclick="AddMore()">Add More</button>
                        </div>
                    </div>    --%>

                        <%--<div class="row">
                        <div class="col-md-4">
                            <br />
                            <label>Processing</label>
                            <label style="color: red; margin-top: 3px;">
                                <b>*</b></label>
                            :
                                       
                                        <select id="selProcessing" class="form-control" onchange="ServiceType()">
                                            <option value="-">-Select-</option>
                                            <option value="1" selected="selected">Normal</option>
                                            <option value="2">Urgent</option>
                                        </select>
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_selProcessing">
                                <b>* This field is required</b></label>
                        </div>
                        <div class="col-md-4">
                            <br />
                            <label>Visa Type</label>
                            <label style="color: red; margin-top: 3px;">
                                <b>*</b></label>:
                                   
                                        <select id="selService" class="form-control" onchange="Notification()">
                                            <option value="-" selected="selected">--Salect Visa Type--</option>
                                            <option value="6">96 hours Visa transit Single Entery (Compulsory Confirm Ticket Copy)</option>
                                            <option value="1">14 days Service VISA</option>
                                            <option value="2">30 days Tourist Single Entry</option>
                                            <option value="3">30 days Tourist Multiple Entry</option>
                                            <option value="4">90 days Tourist Single Entry</option>
                                            <option value="5">90 days Tourist Multiple Entry</option>
                                            <option value="7">90 Days Tourist Single Entry Convertible</option>

                                        </select>
                            <label style="color: red; margin-top: 3px; display: none" id="lbl_selService">
                                <b>* This field is required</b></label>
                        </div>
                    </div>--%>

                        <%--<div class=" row alert alert-info">
                        <div class="col-md-3">
                            <label>Visa Fee</label><br />
                            <i id="Charges" class="Currency"></i>
                            <label style="color: black" id="VisaFee">0.00</label>

                        </div>
                        <div class="col-md-3">
                            <label>Other Fee</label><br />
                            <i id="other" class="Currency"></i>
                            <label style="color: black" id="OtherFee">0.00</label>

                        </div>
                        <div class="col-md-3">
                            <label>Urgent Fee</label><br />
                            <i id="UFee" class="Currency"></i>
                            <label style="color: black" id="UrgentFee">0.00</label>

                        </div>
                        <div class="col-md-2" style="display: none">
                            <label>Service Tax</label><br />
                            <i class="Currency"></i>
                            <label style="color: black" id="ServiceTax">0.00</label>

                        </div>
                        <div class="col-md-3">
                            <label>Total Amount</label><br />
                            <i id="Total" class="Currency"></i>
                            <label style="color: black" id="TotalAmount">0.00</label>
                        </div>
                        <br />
                        <br />
                    </div>--%>

                        <%--<table align="right">
                        <tbody>
                            <tr>
                                <td style="border-top-width: 1px; border-top-style: solid; border-top-color: rgb(255, 255, 255);">
                                    <button id="btn_AddMore" type="button" class="btn btn-default" onclick="AddMore()" title="Add More">
                                        Add More
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>--%>
                    </div>

                <div class="col-xl-3 col-lg-5 col-md-7 col-sm-12 col-12">
                    <div class="sidebar">
                        <div class="widget widget-quote-form bg-yellow">
                            <h3 class="form-title">Summary</h3>

                            <!-- Text input-->
                            <div class="form-group">
                                <div class="">
                                    <label class="">Passanger Count : </label>
                                    <label class="" id="PaxCount"></label>
                                    <br />
                                    <label class="">Visa Price : </label>
                                    <label class="" id="VisaPrice"></label>
                                    * 
                                    <label class="" id="PricePaxCount"></label>
                                    <br />
                                    <label class="" id="lblUrgentPrice">Urgent Price : </label>
                                    <label class="" id="UrgentPrice"></label>
                                    *   
                                    <label class="" id="UrgentPricePaxCount"></label>

                                    <br />
                                    <hr />

                                    <label class="">Total Price : </label>
                                    <label class="" id="TotalPrice"></label>

                                </div>
                            </div>

                            <button type="button" id="RazorButton"  onclick="CheckSessionOnPage()" class="btn btn-default">Pay With RazorPay</button>
                            <%-- <button id="RazorButton"  onclick="GetPrepareRazorPayModal()"  class="btn btn-default">Pay With RazorPay</button>
                                 <button  class="" id="rzp-button1" style="float:left;display:none">Pay</button>--%>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END OF CONTENT -->
</asp:Content>
