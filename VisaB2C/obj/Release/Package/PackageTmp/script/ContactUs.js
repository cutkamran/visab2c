﻿

function EmailSend() {
    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    //var regAddress = new RegExp('^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$');

    var userName = $("#txt_name").val();
    if (userName == "") {
        Success("Please enter User Name");
        return false;
    }
    var nMobile = $("#txt_mobile").val();
    var title = $(".selected-flag");
    title = title[0].title;
    title = title.split(":");
    title = title[1].trim();
    if (nMobile.startsWith("+")) {
        nMobile = nMobile;
    }
    else {
        nMobile = title + " " + nMobile;
    }
  
    if (nMobile == "") {
        bValid = false;
        Success("Please Enter Mobile No");
        return;
    }
    else {
        if (!(reg.test(nMobile))) {
            bValid = false;
            Success("* Mobile no. must be numeric.");
            return;
        }
    }

    var email = $("#txt_email").val();
    if (email == "") {
        Success("Please enter Email");
        return false;
    }
    var subject = $("#txt_subject").val();
    if (subject == "") {
        Success("Please enter Subject");
        return false;
    }
    var message = $("#txt_message").val();
    if (message == "") {
        Success("Please enter Message");
        return false;
    }
    var data = {
        userName: userName,
        nMobile: nMobile,
        email: email,
        subject: subject,
        message: message
    }
    $.ajax({
        type: "POST",
        url: "handler/Contacthandler.asmx/EmailSending",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj == true) {
                alert("Enquiry details send successfully.");
                window.location = "ContactUs.aspx";
            }
            else {
                alert("Something Went Wrong");
            }
        }
    });
}

function BookAssesment() {
    var reg = new RegExp('[0-9]$');
    var regPan = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    var regAddress = new RegExp('^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$');

    var assesmentName = $("#txt_assesmentname").val();
    var assesemail = $("#txt_freeemail").val();
    var mobileno = $("#txt_mobileno").val();
    var visatype = $("#selectVisa").val();
    var assesmessage = $("#message").val();

    if (assesmentName == "") {
        Success("Please enter User Name");
        return false;
    }
    if (mobileno == "") {
        bValid = false;
        Success("Please Enter Mobile No");
        return;
    }
    else {
        if (!(reg.test(mobileno))) {
            bValid = false;
            Success("* Mobile no. must be numeric.");
            return;
        }
    }
    if (assesemail == "") {
        Success("Please enter Email");
        return false;
    }
    if (visatype == "") {
        Success("Please select visa type");
        return false;
    }
    if (assesmessage == "") {
        Success("Please enter assesment message");
        return false;
    }

    var data = {
        assesmentName: assesmentName,
        assesemail: assesemail,
        mobileno: mobileno,
        visatype: visatype,
        assesmessage: assesmessage

    }
    $.ajax({
        type: "POST",
        url: "handler/Contacthandler.asmx/AssesmentEmail",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj == true) {
                alert("Your Book free assesment details send successfully.");
                window.location = "ContactUs.aspx";
            }
            else {
                alert("Something Went Wrong");
            }
        }
    });
}





 

