﻿$(document).ready(function () {
    GetVisa();
});

function GetVisa() {
    //$("#tbl_VisaDetails").dataTable().fnClearTable();
    //$("#tbl_VisaDetails").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../handler/VisaHandler.asmx/GetVisa",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var arrresult = result.Arr;
                if (arrresult.length != 0) {
                    var tRow = '';
                    for (var i = 0; i < arrresult.length; i++) {
                        tRow += '        <tr>';
                        tRow += '    <td style="width:3%" align="center">' + (i + 1) + '</td>';
                        tRow += '    <td style="width:11%" align="center">' + arrresult[i].FirstName + '</td>';
                        tRow += '    <td style="width:11%" align="center">' + arrresult[i].LastName + '</td>';
                        tRow += '    <td style="width:10%" align="center">' + arrresult[i].IeService + '</td>';
                        tRow += '    <td style="width:10%" align="center">' + arrresult[i].Vcode + '</td>';
                        tRow += '<td style="width:10%" align="center">' + arrresult[i].PassportNo + '</td>';
                        tRow += '<td style="width:10%" align="center">' + arrresult[i].ExpDate + '</td>';
                        tRow += '<td style="width:10%" align="center">' + arrresult[i].ArrivalDate + '</td>';
                        tRow += '<td style="width:10%" align="center">' + arrresult[i].DepartingDate + '</td>';
                        tRow += '<td style="width:10%" align="center">' + arrresult[i].VisaFee + '</td>';
                        tRow += '<td style="width:10%" align="center">' + arrresult[i].TotalAmount + '</td>';
                        tRow += '</tr>';
                    }
                    $("#tbl_VisaDetails").append(tRow);
                }
                $("#tbl_VisaDetails").dataTable({

                });

            }
            else {
                alert("An error occured !!!");
            }
        },
        error: function () {
            alert("An error occured !!!");
        }
    });
}