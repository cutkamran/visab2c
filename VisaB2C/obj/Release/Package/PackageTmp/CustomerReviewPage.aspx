﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="CustomerReviewPage.aspx.cs" Inherits="VisaB2C.CustomerReviewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                        <br /><br /><br /><br />
    <!-- /.header classic -->
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <h1 class="page-title">Customer Review page</h1>
                    <p class="page-description">Simple but powerful customer testimonial &amp; review page design template.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Customer Review page</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page breadcrumb -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img"><img src="images/card-testimonial-img-1.jpg" alt="" class="rounded-circle"></div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">“Fusce non mi at nisl laoreet pretiumulla ut elementum sapien, a pulvinar augueed semper sed tellus in ultrices am simply dummy content hendrerit elit vel urna fermentum congue. . ”</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag"><img src="images/country/canada.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Dustin A. Morgan</h4>
                            <p><small>( Canada Students Visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img"><img src="images/card-testimonial-img-2.jpg" alt="" class="rounded-circle"></div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">“Nulla facilisi. Phasellus maximus odio bibendum tortor scelerisque, quis fermentum nulla ornare. Praesent malesuada lorem nec mauris vulputate, viverra tempus felis consequat.”</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag"><img src="images/country/united-states-of-america.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Sandra M. Lebrun</h4>
                            <p><small>( USA PR visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img"><img src="images/card-testimonial-img-3.jpg" alt="" class="rounded-circle"></div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">“Sed sollicitudin, ex id lacinia sagittis, lacus lacus ruNulla in ex ut orci pretium congue quis id nunc. trum lus nulla nunc rutrum est. Pellentesque sit amet elementum risus.”</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag"><img src="images/country/united-kingdom.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Roger N. Towle</h4>
                            <p><small>( UK Tier 1 visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img"><img src="images/card-testimonial-img-1.jpg" alt="" class="rounded-circle"></div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">"Aenean ultricies pulvinar neque. Etiam rutrum velit id tellus volutpat consecte tasellus a enim sodales, placerat erat egetmus egestas et nulla at, gravida accumsan libero."</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag"><img src="images/country/germany.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Dustin A. Morgan</h4>
                            <p><small>( Germany visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img"><img src="images/card-testimonial-img-2.jpg" alt="" class="rounded-circle"></div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">"Quisque luctus neque aliquam dapibus semper. Curabitur luctus mi quis mauris laoreet, nec facilisis est ultricesm hendrerit elit vel urna fermentum congue. . ”"</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag"><img src="images/country/united-kingdom.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Sandra M. Lebrun</h4>
                            <p><small>( USA PR visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="card-testimonial card-testimonial-light">
                        <!-- card testimonial start -->
                        <div class="card-testimonial-img"><img src="images/card-testimonial-img-3.jpg" alt="" class="rounded-circle"></div>
                        <div class="card-testimonial-content">
                            <p class="card-testimonial-text">"Phasellus a enim sodales, placerat erat eget, malesuada nisi. Vivamus lacus risus, egestas et nulla at, gravida accumsan libero. In hac habitasse platea dictumst."</p>
                        </div>
                        <div class="card-testimonial-info">
                            <span class="card-testimonial-flag"><img src="images/country/south-africa.svg" class="flag-small"></span>
                            <h4 class="card-testimonial-name">Roger N. Towle</h4>
                            <p><small>( UK Tier 1 visa )</small></p>
                        </div>
                    </div>
                    <!-- /.card testimonial start -->
                </div>
            </div>
        </div>
    </div>
</asp:Content>
