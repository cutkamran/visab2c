﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Malaysia-E-30Days-Visa.aspx.cs" Inherits="VisaB2C.Malaysia_E_30Days_Visa" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Malaysia E 30 Days Visa</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="best Visa immigration business consulting ">
    <meta name="keywords" content="consulting ">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="1025989426784-pmfpoftgfplpendosl8fmjdmos0ke635.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="script/GoogleSignIn.js"></script>
    <script src="script/CheckSession.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- FontAwesome 4.0 CSS -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <!-- Google Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700%7cPT+Serif:400,400i,700,700i" rel="stylesheet">
    <!-- owl.carousel.min.css -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">


    <!-- fontello.css -->
    <link rel="stylesheet" type="text/css" href="css/fontello.css">
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="header-transparent">
        <div class="topbar-transparent">
            <!-- topbar transparent-->
            <%--  <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-none d-sm-none d-lg-block d-xl-block">
                        <p class="welcome-text">Welcome to Visacafe a immigration company </p>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="header-block">
                            <span class="header-link d-none d-xl-block d-md-block"><a href="#">Talk to Our Expert</a></span>
                            <span class="header-link">+1 800 123 4567</span>
                            <span class="header-link">
                                <button type="submit" class="" data-toggle="modal" data-target="#searchModal"><i class="fa fa-search text-white"></i></button>
                            </span>
                            <span class="header-link"><a href="#" onclick="SignInModel()" class="btn btn-default btn-sm">Sign In</a></span>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
        <!-- /.topbar transparent-->
        <!-- header classic -->
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                    <a href="Default.aspx" class="logo">
                        <img src="images/logo-3.png" alt="Visacafe an Immigration and Visa Consulting "></a>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
                    <div id="navigation-transparent" class="navigation-transparent">
                        <!-- navigation -->
                        <ul>
                            <li>
                                <a href="Default.aspx">Home</a>

                            </li>

                            <li>
                                <a href="#">Visas</a>
                                <ul>
                                    <%--  <li><a href="VisaImmigrationList.aspx">Visa List Page</a></li>--%>
                                    <li><a href="VisaImmigrationSingle.aspx">Malaysia E 30 Days Visa</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">OTB</a>
                            </li>
                            <li>
                                <a href="AboutUs.aspx">About Us </a>
                            </li>
                            <li>
                                <a href="ContactUs.aspx">Contact Us </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <h1 class="page-title">Malaysia E 30 Days Visa</h1>
                    <p class="page-description">Making an excellent website header design is very important. Page header is an excellent way to deliver a message. Make the website header active.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="page-breadcrumb">
        <!-- page breadcrumb -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">About Malaysia E 30 Days Visa</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- /.page breadcrumb -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="content-area">
                        <img src="images/SLIDER/qatar.jpg" alt="" class="img-fluid mb30">
                        <h2>About Malaysia E 30 Days Visa</h2>
                        <p class="lead">Aenean mauris urna, rutrum id luctus ac, sagittis a risus. Suspendisse potenti. Ut feugiat urna in lacinia pretium. Vestibulum sapien dui, tincidunt at lobortis non, pulvinar nec libero. Pellentesque sagittis euismod purus, eget lacinia tellus sagittis quis.</p>
                        <p>Ut hendrerit volutpat dictum. Sed tristique finibus felis, eu posuere lorem posuere ut. Donec et nunc dolor. Nulla tempor nec risus in bibendum. Integer vel nisl pharetra, efficitur dolor ac, lacinia ex. Etiam odio purus, fermentum a lorem at, porttitor posuere sem. </p>
                        <p>Phasellus ex lorem, sollicitudin a placerat nec, pharetra ac sapien. Maecenas venenatis, ex eu condimentum pharetra, leo risus mattis arcu, at ullamcorper mi nisi ac lacus. Fusce felis nunc, malesuada sit amet justo vitae, cursus pretium justo.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="sidebar">
                        <div class="widget-testimonial">
                            <div class="card-testimonial card-testimonial-light">
                                <!-- card testimonial start -->
                                <div class="card-testimonial-img">
                                    <img src="images/card-testimonial-img-1.jpg" alt="" class="rounded-circle">
                                </div>
                                <div class="card-testimonial-content">
                                    <p class="card-testimonial-text">“Fusce non mi at nisl laoreet pretiumulla ut elementum sapien, a pulvinar augueed semper sed tellus in ultrices am simply dummy content hendrerit elit vel urna fermentum congue. . ”</p>
                                </div>
                                <div class="card-testimonial-info">
                                    <span class="card-testimonial-flag">
                                        <img src="images/country/canada.svg" class="flag-small"></span>
                                    <h4 class="card-testimonial-name">Dustin A. Morgan</h4>
                                    <p><small>( Canada Students Visa )</small></p>
                                </div>
                            </div>
                            <!-- /.card testimonial start -->
                        </div>
                        <a onclick="SignInModel('Malaysia E-Entry 30 Days Visa','4500','Malaysia')" class="btn btn-default">Apply For Visa</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="content-area">
                    <p>Duis malesuada, diam vel posuere efficitur, odio elit pretium velit, id tincidunt arcu odio sit amet erat. Nulla maximus nulla eget nunc gravida, tincidunt mollis libero pharetra. </p>
                    <h3>Malaysia E 30 Days Visa Eligibility</h3>
                    <p>Maecenas id leo efficitur, ultrices odio nec, vulputate sem. Donec porta sed arcu nec ultricies. Duis ex elit, vulputate et efficitur nec, condimentum in risus. </p>
                    <p>Fusce non mi at nisl laoreet pretium. Nulla ut elementum sapien, a pulvinar augue. Sed semper sed tellus in ultrices. Nam hendrerit elit vel urna fermentum congue. Aenean varius euismod quam sed ultrices. Duis ac magna turpis. </p>
                    <h3>Malaysia E 30 Days Visa Benefits</h3>
                    <h3>How to Apply</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">1</h2>
                            <p>Fill up short information and upload your documents</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">2</h2>
                            <p>Your application will be reviewed and processed</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                    <!-- How it work card -->
                    <div class="card-how-it-work">
                        <div class="card-how-it-work-body">
                            <h2 class="number-cirle">3</h2>
                            <p>You will be notified once visa is issued,you can print e-visa & fly</p>
                        </div>
                        <div class="slanting-pattern-small"></div>
                    </div>
                </div>
                <!-- /.How it work card -->
            </div>
        </div>
        <div class="space-medium pdb0">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-1 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="faq-header">
                            <h2 class="faq-title">Visa &amp; Immigration</h2>
                            <p>Nulla facilisis elit libero, non pretium orci interdum eget.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-xl-1 col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div id="accordion-2">
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingFour">
                                    <h5 class="mb0">
                                        <button class="accordion-btn collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            How long does it take to get a visa to work in Australia?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <p>Donec eget scelerisque enim. Vestibulum convallis lorem nisi, et blandit massa dapibus ut. Donec tristique sapien est, a feugiat dui interdum ut. Nulla convallis sapien nec orci pellentesque sagittis.</p>
                                        <p>Nulla tempus lectus a elementum rhoncus. Maecenas non feugiat arcu, ut luctus metus. Quisque in ex Donec eget scelerisque enim. Vestibulum convallis lorem nisi, et blandit massa dapibus ut. Donec tristique sapien est, a feugiat dui interdum ut.at velit vestibulum malesuada id et sem.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingFive">
                                    <h5 class="mb0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            What elementum erat at turpis porttitor luctus?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <p>Cras sollicitudin tellus ac tellus tincidunt lacinia. Vestibulum in orci dictum, scelerisque sapien vel, bibendum purus. Praesent purus eros, pretium sollicitudin ex et, finibus hendrerit diam.</p>
                                        <p>In nulla turpis, sodales quis magna at, fermentum auctor elit. Ut egestas turpis dui, in fringilla libero feugiat ut. Vivamus non laoreet risus. Suspendisse sodales nisi in eros suscipit, vitae blandit nunc fermentum. Phasellus iaculis interdum ipsum, non ullamcorper tellus blandit et.</p>
                                        <p>Maecenas sed tortor bibendum, efficitur magna nec, consectetur est. Donec in erat ac urna ullamcorper suscipit scelerisque non lorem. Fusce pharetra libero in velit aliquam placerat. Donec eget scelerisque enim.</p>
                                        <p>Vestibulum convallis lorem nisi, et blandit massa dapibus ut. Donec tristique sapien est, a feugiat dui interdum ut. Nulla convallis sapien nec orci pellentesque sagittis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-accordion">
                                <div class="card-accordion-header" id="headingSix">
                                    <h5 class="mb-0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            How ellentesque quam felis, tempor vel ante a, viverra luctus elit?
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion-2" style="">
                                    <div class="card-accordion-body">
                                        <p>Fusce consequat blandit massa ac viverra. Vivamus malesuada massa dui, feugiat porta sem cursus sed. Quisque diam odio, pretium sit amet tristique a, commodo vitae nunc. Maecenas eu velit libero. Nulla facilisi. Etiam eros sapien, bibendum ac molestie sed, aliquam eu nisi. </p>
                                        <p>Aenean lobortis vulputate diam, quis mattis tellus tincidunt a. Duis hendrerit sodales laoreet. Mauris rhoncus augue a fringilla lacinia. Vestibulum orci nibh, lacinia non viverra sit amet, tristique sed est.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-dark">
        <!-- Footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <%-- <h3 class="widget-title">Important Links</h3>--%>
                        <div class="box">
                            <img src="images/sslcertificate.png" width="100" height="79" alt="Secure Payment">
                            <img src="images/secure-transaction.png" width="100" height="79" alt="Secure Transaction">
                        </div>
                        <div class="box">
                            <img src="images/godaddy.gif" />
                        </div>
                        <br />
                        <div>
                            <img src="images/mastercard.png" />
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <h3 class="widget-title">Visas</h3>
                        <ul class="listnone arrow-footer">
                            <li><a href="#">30 Days Single Entry </a></li>
                            <li><a href="#">30 Days Multiple Entry </a></li>
                            <li><a href="#">90 Days Single Entry </a></li>
                            <li><a href="#">90 Days Multiple Entry </a></li>
                            <li><a href="#">14 Days Single Entry Bahrain </a></li>
                            <li><a href="#">14 Days Multiple Entry Bahrain</a></li>
                            <%--  <li><a href="#">Oman 10 Days Tourist Visa</a></li>
                            <li><a href="#">Oman 01 year Tourist Visa Multiple Entry</a></li>--%>
                        </ul>
                    </div>
                </div>
                <%--  <div class="col-xl-2 col-lg-2 col-md-4 col-sm-3 col-6">
                    <div class="widget-footer">
                        <h3 class="widget-title">Contact Us</h3>

                        <p>
                            1800 102 4150
                            <br>
                            1800 102 4151
                        </p>
                        <p>
                            <a href="#">Schedule a Meeting</a>
                            <br>
                            <a href="#">Talk to our Expert</a>
                        </p>
                    </div>
                </div>--%>
                <div class="col-xl-2 col-lg-2 col-md-4 col-sm-2 col-6">
                    <div class="widget-footer widget-social">
                        <h3 class="widget-title">Connect</h3>
                        <ul class="listnone">
                            <li><a href="https://www.facebook.com/Vizacafe-2285529498390513"><i class="fa fa-facebook social-icon"></i>Facebook</a></li>
                            <li><a href="https://twitter.com/Viza16530470"><i class="fa fa-twitter social-icon"></i>Twitter</a></li>
                            <li><a href="https://www.instagram.com/vizacafe2080/"><i class="fa fa-instagram social-icon"></i>Instagram</a></li>
                            <li><a href="https://www.youtube.com/channel/UCJUtR8T3rX0iFDttSFZ8fbQ"><i class="fa fa-youtube social-icon"></i>Youtube</a></li>
                            <li><a href="#"><i class="fa fa-linkedin social-icon"></i>Linked In</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="widget-footer">
                        <h3 class="widget-title">GET IMMIGRATION TIPS</h3>
                        <%--  <p>Sign up for our Newsletter and join us on the path to success.</p>--%>
                        <form method="post" action="#">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="newsletteremail">Email</label>
                                    <input type="email" class="form-control" id="newsletteremail" name="newsletteremail" placeholder="Enter Email Address">
                                </div>

                            </div>
                            <button type="submit" class="btn btn-default">Sign UP</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.footer -->
    <div class="tiny-footer-dark">
        <!-- tiny footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <p>Copyright © 2018 vizacafe.com | All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <!-- menumaker js -->
    <script src="js/menumaker.js"></script>
    <script src="js/navigation.js"></script>
    <!-- owl.carousel.min.js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom-carousel.js"></script>
    <!--Magnific-Video-Popup-->
</body>
</html>
